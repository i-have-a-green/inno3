<?php

add_action('acf/init', 'acf_init_blocs_listing_proj');
function acf_init_blocs_listing_proj() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc listing projets',
                'title'				    => __('Bloc listing projets'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/listing-project/archive-project.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'blog',
                                            'liste',
                                            'article'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}