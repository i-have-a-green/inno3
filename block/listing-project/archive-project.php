
<?php
/**
 * Block name: Listing project
 */
$term_blog_type = get_term_by( 'slug', get_query_var( 'term' ), 'project-type' );
?>

<div class="postContainer alignwide">
    <?php

        if($term_blog_type){
            $args = array(
                'posts_per_page'    => -1,
                'post_type'         => 'projets',
                'post_status'       => 'publish',
                'tax_query'         => array(
                    array(
                        'taxonomy' => 'project-type',
                        'field'    => 'slug',
                        'terms'    => $term_blog_type->slug,
                    )
                )
            );
        }
        else{
            $args = array(
                'posts_per_page'    => -1,
                'post_type'         => 'projets',
                'post_status'       => 'publish',
            );
        }
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                get_template_part( 'block/cards/'. get_post_type() .'-card' );
            }
        }
    ?>
</div>
<?php
wp_reset_postdata();
?>
