
<?php
/**
 * Block name: Listing team
 */
?>
<div class="filters">
    <form method="POST" permalink="<?php the_permalink();?>" id="form_team">
        <?php
            $blogusers = get_users( );
            $tabSkill = [];
            $tabSituation = [];
            $id_en_poste = 0;
            foreach ( $blogusers as $user ) {
                $skills = get_field('skills', $user);
                $situations = get_field('situation', $user);
                if($skills){
                    foreach ($skills as $skill) {
                        if(!in_array($skill, $tabSkill)){
                            array_push($tabSkill, $skill);
                        }
                    }
                }   
                if($situations){
                    foreach ($situations as $situation) {
                        if(!in_array($situation, $tabSituation)){
                            array_push($tabSituation, $situation);
                        }
                        if($id_en_poste == 0){
                            $situationTerm = get_term($situation);
                            if($situationTerm->name == 'En poste' || $situationTerm->name == 'In board'){
                                $id_en_poste = $situationTerm->term_id;
                            }
                        }
                    }
                }            
            }
        echo '<b>'.__('Filtrer par personne','ihag').'</b>';?>
        <input type="radio" name="sit" value="" id="sit_0" <?php if(isset($_POST['sit'])){checked("", $_POST['sit']);}?>><label class="button-search" for="sit_0"><?php echo esc_attr('Tous');?></label>
        <?php
        foreach ($tabSituation as $situation) {
            $situation = get_term($situation);
            ?>
            <input type="radio" name="sit" value="<?php echo esc_attr( $situation->term_id ); ?>" id="sit_<?php echo $situation->term_id;?>"  <?php if(isset($_POST['sit'])){checked($situation->term_id, $_POST['sit']);}elseif($situation->term_id == $id_en_poste){echo 'checked';}?>><label class="button-search" for="sit_<?php echo $situation->term_id;?>"><?php echo esc_attr( $situation->name ); ?></label>
            <?php
        }
        ?>  
        <!--et/ou par compétences :
        <select name="skill">
            <option value=""><?php _e('Tous','ihag');?></option>
            <?php foreach ($tabSkill as $skill) {
                $skill = get_term($skill);
                ?>
                <option value='<?php echo $skill->term_id;?>'  <?php if(isset($_POST['skill'])){selected($skill->term_id, $_POST['skill']);}?> ><?php echo $skill->name;?></option>
                <?php
            } ?>
        </select>-->
        <!--<div class="sub-container">-->
            <input class="submiter" type="submit" value="Filtrer">
        <!--</div> -->
    </form>
</div>

<div class="postContainer">
    <?php
        $args = array ( 'orderby' => 'nicename' );
        $users = get_users( $args );
        foreach($users as $user){
            $display_user = true;            
            /*if (isset($_POST['sit']) && isset($_POST['skill'])) {
                $display_user = false;
                $user_skill = get_field('skills','user_'.$user->ID);
                $user_sit = get_field('situation','user_'.$user->ID);
                if (!empty($_POST['skill']) && !empty($_POST['sit'])) {
                    if (in_array($_POST['skill'], $user_skill) && in_array($_POST['sit'], $user_sit)) {
                        $display_user = true;
                    }
                }else if (!empty($_POST['skill'])) {
                    if (in_array($_POST['skill'], $user_skill)) {
                        $display_user = true;
                    }
                }else if (!empty($_POST['sit'])) {
                    if (in_array($_POST['sit'], $user_sit)) {
                        $display_user = true;
                    }
                }else{
                    $display_user = true;
                }
            }*/
            $display_user = false;
            $user_sit = get_field('situation','user_'.$user->ID);
            if (isset($_POST['sit'])) {
                if (!empty($_POST['sit'])) {
                    if (in_array($_POST['sit'], $user_sit)) {
                        $display_user = true;
                    }
                }else{//dans le cas ou c'est TOUS
                    $display_user = true;
                }
            }
            else if (in_array($id_en_poste, $user_sit)) {
                $display_user = true;
            }

            if ($display_user) {
                set_query_var( 'user', $user);
                get_template_part( 'block/cards/team-card-listing' );
            }
        }
    ?>
</div>