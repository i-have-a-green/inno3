<?php

add_action('acf/init', 'acf_init_blocs_listing_team');
function acf_init_blocs_listing_team() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'team_listing',
                'title'				    => __('Bloc listing Équipe'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/listing-team/archive-team.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'team',
                                            'liste',
                                            'Équipe'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}