<?php

add_action('acf/init', 'acf_init_blocs_listing_real_about');
function acf_init_blocs_listing_real_about() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc listing Réalisations About',
                'title'				    => __('Bloc listing Réalisation About'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/listing-real-about/archive-real.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'blog',
                                            'liste',
                                            'article'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}