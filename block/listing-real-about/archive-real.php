
<?php
/**
 * Block name: Listing réalisation
 */

?>

<div class="postContainerAbout alignwide real-listing-about">
    <?php
        if( have_rows('realisations') ):
            global $post;
            while( have_rows('realisations') ) : the_row();

                $itemID = get_sub_field('realisation');
                $post = get_post($itemID);
                setup_postdata($post);
                get_template_part( 'block/cards/'. get_post_type() .'-card' );
            endwhile;
            wp_reset_postdata();
        endif;
    ?>
</div>