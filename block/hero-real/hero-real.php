
<?php
/**
 * Block name: Hero realisations
 */

global $post;
$terms = get_the_terms($post->ID, 'real-type');
$term = false;
if (isset($terms[0])) {
    $term = $terms[0];
} 
$tags = get_the_terms($post->ID, 'global_tags' );

?>
<div class="wp-block-columns alignwide single-header <?php echo ($term)?$term->slug:'Réalisation';?>" style="gap:1.5rem;">
    <div class="wp-block-column" style="flex-basis:35%">
        <figure class="wp-block-post-featured-image">
            <?php the_post_thumbnail('450-450');?>
            <div class="real-term">
                <p><?php echo ($term === false)?'Réalisation':esc_attr( $term->name ); ?></p>
            </div>
        </figure>
    </div>
    <div class="wp-block-column">
        <?php wpBreadcrumb();?>
        <h1 class="wp-block-post-title">
            <?php echo esc_attr($post->post_title);?>
        </h1>
        <div class="has-background has-color-white-background-color wp-block-post-date"><?php the_date()?></div>
        <div class="real-tags">
            <?php
                foreach ($tags as $tag) {
                    echo '<a href="'.get_permalink(get_field('search_page','option')).'?var_global_tags='.$tag->term_id.'"><p>'.$tag->name.'</p></a>';
                }
            ?>
        </div>
    </div>
</div>