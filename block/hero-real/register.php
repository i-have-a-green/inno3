<?php

add_action('acf/init', 'acf_init_blocs_hero_real');
function acf_init_blocs_hero_real() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'hero_real',
                'title'				    => __('Entête réalisation'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/hero-real/hero-real.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'team',
                                            'liste',
                                            'Équipe'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}