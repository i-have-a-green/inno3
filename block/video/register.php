<?php

add_action('acf/init', 'acf_init_blocs_video');
function acf_init_blocs_video() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc Vidéo',
                'title'				    => __('Bloc Vidéo'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/video/video.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'enqueue_script'        => 'parts/block/video/video.js',
                'keywords'			    => array(
                                            'vidéo',
                                            'video',
                                            'youtube'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}