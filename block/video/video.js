let modaleActivator = document.getElementsByClassName('modaleActivator')

Array.prototype.forEach.call(modaleActivator, function (el) {
    el.addEventListener('click',function (e) {

        e.stopPropagation()

        // Récupération de l'iframe
        let oembed = document.getElementById('dataContainer').dataset.video

        // Création de mon conteneur de modale -> BG de fermeture
        let modaleContainer = document.createElement("div")
        modaleContainer.classList.add('modaleContainer')

        // Creation de ma modale contenant l'iframe avec class
        let modale = document.createElement("div")
        modale.innerHTML = oembed
        modale.classList.add('modale')

        // Création du btn de fermeture avec class
        let modaleClose = document.createElement("button")
        modaleClose.classList.add('modaleCloseBtn')
        modaleClose.innerText = '✗'

        // Insertion dans le DOM des différents éléments
        modaleContainer.appendChild(modale)
        modaleContainer.appendChild(modaleClose)
        document.body.appendChild(modaleContainer)

        document.body.style.overflow = 'hidden'

        // Gestion de la fermeture du modale
        document.addEventListener('click', function(e) {
            if ( !modale.contains(e.target) ) {
                modaleContainer.remove();
                document.body.style.overflow = 'visible'
            }
        })
    })
})