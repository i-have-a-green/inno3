<?php

add_action('acf/init', 'acf_init_blocs_listing_orga');
function acf_init_blocs_listing_orga() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc listing organisations',
                'title'				    => __('Bloc listing orga'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/listing-organisations/archive-orga.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'orga',
                                            'liste',
                                            'organisations'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}