
<?php
/**
 * Block name: Listing orga
 */

if(get_field('organisation') != 'references'):
?>

<div class="filters alignwide orga-filters">
    <?php
        $terms = get_terms( array( 'taxonomy' => get_field('organisation') ) );
        if ( $terms ) {
            echo esc_attr( 'Filter par organisations ' );
            echo '<div class="post-tags-filters">';?>
            <button class="all active-button"><?php echo esc_attr( 'Tous' ); ?></button>
            <?
            foreach ( $terms as $term_post_tag ) :
                ?>
                    <button class="organisation-<?php echo esc_attr( $term_post_tag->slug); ?>"><?php echo esc_attr( $term_post_tag->name ); ?></button>
                <?php 
            endforeach;?>
            <?php
            echo '</div>';
        }
    ?>
    <input class="submiter" type="submit" value="Filtrer">  
</div>
<?php endif;?>
<div class="postContainer alignwide orga-container">
    <?php
        $taxonomy = get_field('organisation');
        $taxonomy_terms = get_terms( $taxonomy, array('fields' => 'ids') );
        $organisations = get_posts( array(
            'numberposts'    => -1,
            'post_type'         => 'organisations',
            'post_status'       => 'publish',
            'tax_query'         => array(
                        array(
                            'taxonomy' => $taxonomy,
                            'field' => 'id',
                            'terms' => $taxonomy_terms,
                        ),
            ),
        ) );

        global $post;
        foreach($organisations as $organisation){
            $post = get_post($organisation->ID);
            $terms = get_the_terms( $post, get_field('organisation') );
            $class = '';
            if ( $terms ) {
                foreach ( $terms as $term_post_tag ) :
                    $class=' organisation-'.$term_post_tag->slug;
                endforeach;
            }
            set_query_var( 'class_term', $class);
            get_template_part( 'block/cards/'. get_post_type() .'-card' );
        }
        
    ?>
</div>