
<?php
/**
 * Block name: search
 */

?>
<div class="filters alignwide">
    <form method="POST" permalink="<?php the_permalink();?>" style="width:100%">
        <div class="form-group" id="searchbar">
            <p class="search-indication"><?php _e('Requête recherchée','ihag');?></p>
            <div>
                <input placeholder="Rechercher..." type="search" name="search" value="<?php echo (isset($_POST['search'])) ? $_POST['search'] : '';?>">
                <input type="submit" value="rechercher">
            </div>
        </div>

        <!--<input type="radio" id="cpt_all" name="cpt" value="" <?php checked('', $_POST['cpt']);?>><label for="cpt_all">Tous</label>
        <input type="radio" id="cpt_post" name="cpt" value="post" <?php checked('post', $_POST['cpt']);?>><label for="cpt_post">Article</label>
        <input type="radio" id="cpt_event" name="cpt" value="event" <?php checked('event', $_POST['cpt']);?>><label for="cpt_event">Evennement</label>
        <input type="radio" id="cpt_job" name="cpt" value="job" <?php checked('cpt_job', $_POST['cpt']);?>><label for="cpt_job">Job</label>-->
        <div class="search-filters">
            <div class="container-1">
            <?php
                _e('Filtrer par type de contenu ');
                ?>
                <div class="inputs">
                <input type="radio" name="cpt" value="" id="cpt_0" <?php if(isset($_POST['cpt'])){checked("", $_POST['cpt']);}else{echo 'checked';}?>><label class="button-search" for="cpt_0"><?php echo esc_attr('Tous');?></label>
                <?php
                    $postType = get_post_type_object('realisations');
                    $postName = esc_html($postType->labels->singular_name);
                ?>
                <input type="radio" name="cpt" value="realisations" id="cpt_realisations"  <?php if(isset($_POST['cpt'])){checked('realisations', $_POST['cpt']);}?>><label class="button-search" for="cpt_realisations"><?php echo $postName;?></label>

                <?php
                    $postType = get_post_type_object('news');
                    $postName = esc_html($postType->labels->singular_name);
                ?>
                <input type="radio" name="cpt" value="news" id="cpt_news"  <?php if(isset($_POST['cpt'])){checked('news', $_POST['cpt']);}?>><label class="button-search" for="cpt_news"><?php echo $postName;?></label>
                <?php
                /*$terms = get_terms( array( 'taxonomy' => 'blog-type' ) );
                if ( $terms ) {
                    ?>
                    <input type="radio" name="cpt" value="" id="cpt_0" <?php if(isset($_POST['cpt'])){checked("", $_POST['cpt']);}else{echo 'checked';}?>><label class="button-search" for="cpt_0"><?php echo esc_attr('Tous');?></label>
                    <?php
                    foreach ( $terms as $term_tag ) :
                        ?>
                        <input type="radio" name="cpt" value="<?php echo esc_attr( $term_tag->term_id ); ?>" id="cpt_<?php echo $term_tag->term_id;?>"  <?php if(isset($_POST['cpt'])){checked($term_tag->term_id, $_POST['cpt']);}?>><label class="button-search" for="cpt_<?php echo $term_tag->term_id;?>"><?php echo esc_attr( $term_tag->name ); ?></label>
                        <?php 
                    endforeach; ?>
                    <?php 
                }*/
            ?>
            </div>
            </div>
            <div class="container-2">
            <?php
                $terms = get_terms( array( 'taxonomy' => 'global_tags' ) );
                if ( $terms ) {
                    echo esc_attr( 'et/ou par thématiques ' );
                    echo '<select name="var_global_tags">';
                    $select = '';
                    if (isset($_POST['var_global_tags']) || isset($_GET['var_global_tags'])) {
                        $var_global_tags = isset($_POST['var_global_tags'])?$_POST['var_global_tags']:$_GET['var_global_tags'];
                    }
                    if (isset($var_global_tags)) {
                        $select = selected('', $var_global_tags);
                    }
                    echo '<option value="" '. $select .'>Tous les tags</option>';
                    foreach ( $terms as $term_tag ) :
                        $select = '';
                        if (isset($var_global_tags)) {
                            $select = selected($term_tag->term_id, $var_global_tags);
                        }
                        ?>
                        <option value="<?php echo esc_attr( $term_tag->term_id ); ?>" <?php echo $select;?>><?php echo esc_attr( $term_tag->name ); ?></option>
                        <?php 
                    endforeach; ?>
                    <?php 
                    echo '</select>';
                }
            ?>
            </div>
            <div class="sub-container"><input class="submiter" type="submit" value="Filtrer"></div>
        </div>
    </form>
</div>

<div class="postContainer alignwide">
    <?php
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

        if(isset($_POST['cpt']) && !empty($_POST['cpt'])){
            $args = array(
                'paged'             => $paged,
                'post_type'         => sanitize_text_field($_POST['cpt']),
                'post_status'       => 'publish',
            );
        }
        else{
            $args = array(
                'paged'             => $paged,
                'post_type'         => array('realisations', 'news'),
                'post_status'       => 'publish',
            );
        }
        if (isset($_POST['var_global_tags']) || isset($_GET['var_global_tags'])) {
            $var_global_tags = isset($_POST['var_global_tags'])?$_POST['var_global_tags']:$_GET['var_global_tags'];
            if(!empty($var_global_tags)){
                $args = array(
                    'paged'             => $paged,
                    'post_type'         => array('projets', 'organisations', 'realisations', 'news'),
                    'post_status'       => 'publish',
                );
                $args['tax_query'][] = array(
                    'taxonomy' => 'global_tags',
                    'field'    => 'term_id',
                    'terms'    => (int)$var_global_tags,
                );
            }
        }

        if(isset($_POST['search']) && !empty($_POST['search'])){
            $args['s'] = sanitize_text_field($_POST['search']);
        }

        query_posts( $args );
        while ( have_posts() ) : the_post();
            get_template_part( 'block/cards/'. get_post_type() .'-card' );
        endwhile;
    ?>
</div>
<?php
    the_posts_pagination();
    wp_reset_query();
?>
