<?php

add_action('acf/init', 'acf_init_blocs_search');
function acf_init_blocs_search() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc search',
                'title'				    => __('Bloc recherche'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/search/search.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'recherche',
                                            'search'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}