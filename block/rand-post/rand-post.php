
<?php
/**
 * Block name: Random post
 */
?>
<div>
	<?php
		$articlesRand = get_posts( array(
			'posts_per_page'    => 1,
			'post_type'         => 'news',
			'post_status'       => 'publish',
/* 			'meta_query'     => array(
				array(
					'key'   => 'display_home',
					'value' => '1', 
				),
			) */
		) );
		global $post;
		foreach($articlesRand as $articleRand){
			$post = get_post($articleRand->ID);
			get_template_part( 'block/cards/'. get_post_type() .'-card' );
		}
		wp_reset_postdata();
	?>
</div>