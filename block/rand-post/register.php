<?php

add_action('acf/init', 'acf_init_blocs_rand_post');
function acf_init_blocs_rand_post() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc random Post',
                'title'				    => __('Bloc random post'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/rand-post/rand-post.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'blog',
                                            'liste',
                                            'article',
                                            'aléatoire'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}