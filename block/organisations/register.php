<?php

add_action('acf/init', 'acf_init_blocs_organisations');
function acf_init_blocs_organisations() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc organisations',
                'title'				    => __('Bloc organisations'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/organisations/organisations.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'organisations',
                                            'auteur',
                                            'article',
                                            'aléatoire'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}