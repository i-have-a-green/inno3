<?php
/**
 * Block name: Organisations 
 */

 

if( have_rows('organisations') ):

    while( have_rows('organisations') ) : the_row();
		$organisation = get_sub_field('organisation');
		//var_dump($organisation);

		echo '<a class="organisation" href="'. get_permalink($organisation->ID) .'">';
	
			echo '<p>'.$organisation->post_title.'</p>';

		echo '</a>';
    endwhile;

else :
endif;

?>
