
<?php
/**
 * Block name: single auteur
 */


$author = get_user_by('ID', get_the_author_meta( 'ID' ));
?>
<div class="wp-block-columns aligndefault single-header">
    <div class="wp-block-column" style="flex-basis:20%">
        <figure class="wp-block-post-featured-image auth-pic">
            <?php echo wp_get_attachment_image(get_field('profile-picture', 'user_'.$author->ID), '350-350');?>
            <svg viewBox="0 0 400 400" xmlns="http://www.w3.org/2000/svg">
                <rect x="0" y="0" width="100%" height="100%" />
            </svg>
        </figure>
        <div class="socials">
            <a href="<?php the_field('twitter', 'user_'.$author->ID)?>"><img src="/wp-content/themes/inno3/assets/img/Twitter.svg" alt="<?php _e('Lien ver le twitter de l\'auteur');?>"></a>
            <a href="<?php the_field('mastodon', 'user_'.$author->ID)?>"><img src="/wp-content/themes/inno3/assets/img/Mastodon.svg" alt="<?php _e('Lien ver le mastodon de l\'auteur');?>"></a>
            <a href="<?php the_field('linkedin', 'user_'.$author->ID)?>"><img src="/wp-content/themes/inno3/assets/img/Linkedin.svg" alt="<?php _e('Lien ver le linkedin de l\'auteur');?>"></a>
        </div>
    </div>
    <div class="wp-block-column">
        <?php wpBreadcrumb();?>
        <h1 class="wp-block-post-title">
            <?php echo esc_attr($author->display_name);?>
        </h1>
        <div class="auth-skill">
            <?php
            $tags = get_field('skills', 'user_'.$author->ID);
            foreach ($tags as $tag) {
                echo ('<p>'.$tag->name.'</p>');
            }
            ?>
        </div>
        <div class="wp-block-button is-style-ol-b">
            <a class="wp-block-button__link alignright" href="mailto:<?php echo antispambot($author->user_email,1);?>"><?php _e('Envoyer un mail');?></a>
        </div>
    </div>
</div>


<div class="wp-block-group aligndefault negative-margin">
    <div class="wp-block-columns" style="gap:1.5rem;">
        <div class="wp-block-column">
        </div>
        <div class="wp-block-column">
            <h3 class=""><?php _e('À propos')?></h3>
            <?php $test = the_field("about", 'user_'.$author->ID);?>
        </div>
    </div>
</div>
<div class="wp-block-group aligndefault">
    <div class="wp-block-columns" style="gap:1.5rem;">
        <div class="wp-block-column">
                <h3 class=""><?php _e('Biographie')?></h3>
                <p><?php the_field("biography", 'user_'.$author->ID);?></p>
        </div>
        <div class="wp-block-column">
            <div class="wp-block-group has-border-color has-primary-rust-border-color has-primary-rust-color has-text-color" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem">
                <h3 class="has-primary-rust-color has-text-color"><?php _e('Compétences')?></h3>
                <p><?php the_field("skills_text", 'user_'.$author->ID);?></p>
            </div>
        </div>
    </div>
</div>

<div class="wp-block-columns alignwide" style="gap: 2rem;">
    <div class="wp-container-11 wp-block-column" style="height: 100%;">
        <div class="wp-container-10 wp-block-group has-color-white-background-color has-background" style="height: 100%;border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem">
            <h3 class=""><?php _e('Signes distinctifs')?></h3>
            <p><?php the_field("distinctive", 'user_'.$author->ID);?></p>
        </div>
    </div>
    <div class="wp-container-14 wp-block-column" style="height: 100%;">
        <div class="wp-container-13 wp-block-group has-color-white-background-color has-background" style="height: 100%;border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem">
            <h3 class=""><?php _e('Outils')?></h3>
            <p><?php the_field("tools", 'user_'.$author->ID);?></p>
        </div>
    </div>
    <div class="wp-container-17 wp-block-column" style="height: 100%;">
        <div class="wp-container-16 wp-block-group has-color-white-background-color has-background" style="height: 100%;border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem">
            <h3 class=""><?php _e('Formations')?></h3>
            <p><?php the_field("formation", 'user_'.$author->ID);?></p>
        </div>
    </div>
</div>