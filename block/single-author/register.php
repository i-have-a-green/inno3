<?php

add_action('acf/init', 'acf_init_blocs_single_author');
function acf_init_blocs_single_author() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'single_author',
                'title'				    => __('Single auteur'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/single-author/single-author.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'team',
                                            'liste',
                                            'Équipe'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}