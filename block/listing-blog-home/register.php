<?php

add_action('acf/init', 'acf_init_blocs_listing_blog_home');
function acf_init_blocs_listing_blog_home() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc listing Blog Home',
                'title'				    => __('Bloc listing Blog Home'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/listing-blog-home/archive-blog.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'blog',
                                            'liste',
                                            'article'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}