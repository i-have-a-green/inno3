
<?php
/**
 * Block name: Listing blog
 */

?>

<div class="postContainer alignwide blog-listing-home">
    <?php
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $articles = get_posts( array(
            'paged'             => $paged,
            'posts_per_page'    => 4,
            'post_type'         => 'news',
            'post_status'       => 'publish',
        ) );
        global $post;
        foreach($articles as $article){
            $post = get_post($article->ID);
            get_template_part( 'block/cards/'. get_post_type() .'-card' );
        }
        wp_reset_postdata();
    ?>

</div>