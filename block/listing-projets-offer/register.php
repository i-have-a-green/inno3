<?php

add_action('acf/init', 'acf_init_blocs_projet_offer');
function acf_init_blocs_projet_offer() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc listing CPT au choix',
                'title'				    => __('Bloc listing CPT au choix'),
                'description'		    => __('Uniquement dans la colonne des meta données'),
                'render_template'	    => get_template_directory() . '/block/listing-projets-offer/list-projets-offer.php',
                /* 'mode'                  => 'preview', */
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'blog',
                                            'liste',
                                            'article'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            /* 'mode'      => false, */
                                            'jsx'       => true
                )
            )
        );
    }
}