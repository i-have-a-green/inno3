
<?php
/**
 * Block name: List Projets Offer
 */




?>

<div class="group_content cpt-list alignwide">
    <?php
        if( have_rows('items') ):
            global $post;
            while( have_rows('items') ) : the_row();

                $itemID = get_sub_field('item');
                $post = get_post($itemID);
                setup_postdata($post);
                get_template_part( 'block/cards/'. get_post_type() .'-card' );
            endwhile;
            wp_reset_postdata();
        endif;
    ?>
</div>