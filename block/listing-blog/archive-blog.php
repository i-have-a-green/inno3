
<?php
/**
 * Block name: Listing blog
 */


$term_blog_type = get_term_by( 'slug', get_query_var( 'term' ), 'blog-type' ); 

?>

<div class="filters alignwide">
    <?php
        $terms = get_terms( array( 'taxonomy' => 'blog-type' ) );
        if ( $terms ) { 
            echo '<div class="post-tags-filters">';
            echo esc_attr( 'Filter par type de contenu' );
            ?>
            <a href="<?php echo esc_url( home_url( 'blog' ) ); ?>" <?php echo (!$term_blog_type)?'class="active"':'';?>><?php echo esc_attr( 'Tous' ); ?></a>
            <?php
            foreach ( $terms as $term_post_tag ) :
                $active = '';
                if($term_blog_type && $term_blog_type->term_id == $term_post_tag->term_id){
                    $active = 'class="active"';
                }
                ?>
                    <a href="<?php echo esc_url( get_term_link( $term_post_tag ) ); ?>" <?php echo $active;?>><?php echo esc_attr( $term_post_tag->name ); ?></a>
                <?php 
            endforeach;?>
            <?php 
            echo '</div>';
        }
    ?>
<form permalink="<?php the_permalink();?>" method="POST" id="form_tag">
<?php
    $terms = get_terms( array( 'taxonomy' => 'global_tags' ) );
    if ( $terms ) {
        echo esc_attr( 'Filter par tags' );
        echo '<select name="var_global_tags" >';

        $select = '';
        if (isset($_POST['var_global_tags'])) {
            $select = selected("", $_POST['var_global_tags']);
        }

        echo '<option value="" '. $select .'>Tous les tags</option>';
        foreach ( $terms as $term_tag ) :
            $select = '';
            if (isset($_POST['var_global_tags'])) {
                $select = selected($term_tag->term_id, $_POST['var_global_tags']);
            }
            ?>
            <option value="<?php echo esc_attr( $term_tag->term_id ); ?>" <?php echo $select;?>><?php echo esc_attr( $term_tag->name ); ?></option>
            <?php 
        endforeach; ?>
        <?php 
        echo '</select>';
    }
?>
<div class="sub-container"><input class="submiter" type="submit" value="Filtrer"></div>
</form>
</div>

<div class="postContainer alignwide">
    <?php
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

        if($term_blog_type){
            $args = array(
                'paged'             => $paged,
                'post_type'         => 'news',
                'post_status'       => 'publish',
                'tax_query'         => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'blog-type',
                        'field'    => 'slug',
                        'terms'    => $term_blog_type->slug,
                    )
                )
            );
        }
        else{
            $args = array(
                'paged'             => $paged,
                'post_type'         => 'news',
                'post_status'       => 'publish',
            );
        }

        if(isset($_POST['var_global_tags']) && !empty($_POST['var_global_tags'])){
            $args['tax_query'][] = array(
                'taxonomy' => 'global_tags',
                'field'    => 'term_id',
                'terms'    => (int)$_POST['var_global_tags'],
            );
        }

        global $the_query;
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                get_template_part( 'block/cards/'. get_post_type() .'-card' );
            }
        }
    ?>
</div>
<?php
the_posts_pagination();
wp_reset_postdata();
?>