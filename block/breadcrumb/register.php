<?php

add_action('acf/init', 'acf_init_blocs_video');
function acf_init_blocs_video() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'breadcrumb',
                'title'				    => __('Fil d\'ariane'),
                'description'		    => __(''),
                'render_template'	    => 'block/breadcrumb/breadcrumb.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                //'enqueue_script'        => 'parts/block/video/video.js',
                'keywords'			    => array(
                                            'fil',
                                            'ariane',
                                            'breadcrumb'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}