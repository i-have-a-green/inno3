<?php

add_action('acf/init', 'acf_init_blocs_listing_author');
function acf_init_blocs_listing_author() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc listing_author',
                'title'				    => __('Bloc listing auteur'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/listing-link-author/listing_author.php',
                /* 'mode'                  => 'preview', */
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'blog',
                                            'liste',
                                            'article'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            /* 'mode'      => false, */
                                            'jsx'       => true
                )
            )
        );
    }
}