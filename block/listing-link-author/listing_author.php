
<?php
/**
 * Block name: Liste autheur publications
 */
$author = get_user_by('ID', get_the_author_meta( 'ID' ));
?>
<div class="alignwide">
    <h2><?php _e('Les publication de '.$author->display_name) ?></h2>
    <div class="postContainer alignwide">
        <?php
            if( have_rows('publications', 'user_'.$author->ID) ):
                global $post;
                while( have_rows('publications','user_'.$author->ID) ) : the_row();
                    $itemID = get_sub_field('publication');
                    $post = get_post($itemID);
                    setup_postdata($post);
                    get_template_part( 'block/cards/'. get_post_type() .'-card' );
                endwhile;
                wp_reset_postdata();
            endif;
        ?>
    </div>
</div>