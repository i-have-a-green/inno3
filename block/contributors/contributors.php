<?php
/**
 * Block name: contributeurs
 */

 

if( have_rows('contributors') ):

    while( have_rows('contributors') ) : the_row();
		$contributor = get_sub_field('contributor');
		//var_dump($contributor);

		echo '<a class="contributor" href="'. get_author_posts_url($contributor->ID) .'">';
			$image = get_field('profile-picture', $contributor);
			$size = '50-50';
			if( $image ) {
				echo wp_get_attachment_image( $image, $size );
			}else{
				echo get_avatar( get_the_author_meta( $contributor->ID ), 
					$size = '72',
					); 
			}
			echo '<p>'.$contributor->display_name.'</p>';

		echo '</a>';
    endwhile;

else :
endif;

?>
