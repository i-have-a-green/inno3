<?php

add_action('acf/init', 'acf_init_blocs_contributors');
function acf_init_blocs_contributors() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'Bloc contributeurs',
                'title'				    => __('Bloc contributeur'),
                'description'		    => __(''),
                'render_template'	    => get_template_directory() . '/block/contributors/contributors.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                /* 'enqueue_script'        => './archive-blog.js', */
                'keywords'			    => array(
                                            'contributeur',
                                            'auteur',
                                            'article',
                                            'aléatoire'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
    }
}