<?php
/**
 * Template part for displaying card orga
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */
$terms = '';
if (isset($class_term)) {
    $terms = $class_term;
}
?>

<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" class="<?php echo $terms;?> active animate orga-card-link">
    <article id="post-<?php the_ID(); ?>" class="type-organisations">
        <p class="orga-term"><?php echo esc_attr('Organisation')?></p>
        <div class="post-thumbnail">
        <?php 
                if (get_the_post_thumbnail()) {
                    the_post_thumbnail( '350-350' );
                }else{
                    //echo wp_get_attachment_image(get_field('image_placeholder','option'));
                }
            ?> 
        </div>
        <div class="content">
            <header class="entry-header">
                <?php
                the_title( '<h3 class="entry-title">', '</h3>' );
                ?>
            </header><!-- .entry-header -->
            <?php the_excerpt(); ?>
        </div>
    </article><!-- #post-<?php the_ID(); ?> -->
</a>