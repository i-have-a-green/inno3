<?php
/**
 * Template part for displaying archive team
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */

?>
<a href="<?php echo esc_url( get_author_posts_url( $user->ID ) );?>" rel="bookmark" class="link-card-user">
    <article id="user-<?php echo $user->ID; ?>" class="user-content" >
        <div class="team-term">
            <p><?php echo esc_attr('EQUIPE'); ?></p>
        </div>
        <div class="user-thumbnail">
            <?php
                $image = get_field('profile-picture', $user);
                $size = '350-350';
                if( $image ) {
                    echo wp_get_attachment_image( $image, $size );
                }else{
                    echo get_avatar( get_the_author_meta( $user->ID ), 
                        $size = '150',
                        ); 
                }
            ?>
            <svg viewBox="0 0 400 260" xmlns="http://www.w3.org/2000/svg">
                <rect x="0" y="0" width="100%" height="100%" />
            </svg>
        </div>

        <div class="content">
            <header class="entry-header">
                <h3 class="entry-title"><?php echo $user->display_name;?></h3>
            </header><!-- .entry-header -->

             <?php
            $skills = get_field('skills', $user);
            $skillsTXT = '';
            if($skills){
                foreach ($skills as $skill) {
                    $skill = get_term($skill);
                    $skillsTXT .= $skill->name . ' | ';
                }
            }   
            echo substr($skillsTXT,0, -3);
            ?>
        </div>
    </article><!-- #post-<?php the_ID(); ?> -->
</a>