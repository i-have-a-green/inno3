<?php
/**
 * Template part for displaying archive posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */
$terms = get_the_terms( $post, 'project-type' );

$term = false;
if (isset($terms[0])) {
    $term = $terms[0];
} 

?>

<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" class="project-card-link">
    <article id="post-<?php the_ID(); ?>" <?php ($term !== false)?post_class($term->slug):post_class(); ?>>
        <div class="project-term project-img <?php echo ($term === false )? '': 'project-'.$term->slug;?>">
            <div class="post-thumbnail">
            <?php 
                if (get_the_post_thumbnail()) {
                    the_post_thumbnail( '350-350' );
                }else{
                   // echo wp_get_attachment_image(get_field('image_placeholder','option'));
                }
            ?> 
                <svg viewBox="0 0 400 200" xmlns="http://www.w3.org/2000/svg">
                    <rect x="0" y="0" width="100%" height="100%" />
                </svg>
            </div>
            <p><?php echo ($term === false)?'Projet':esc_attr( $term->name ); ?></p>
        </div>

        <div class="content">
            <header class="entry-header">
                <?php
                the_title( '<h3 class="entry-title">', '</h3>' );
                ?>
            </header><!-- .entry-header -->
        </div>
    </article><!-- #post-<?php the_ID(); ?> -->
</a>