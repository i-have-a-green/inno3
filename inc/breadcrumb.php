<?php

function tplItem($lien, $nom, $pos, $last_item = false, $class=false) {
    if ( $class ) :
      $class = "class='".$class."' ";
    endif;
    $item = "<li ".$class."itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>";
    if ( !$last_item ) :
      $item .= "<a itemprop='item' href='".$lien."'><span itemprop='name'>".$nom."</span></a>";
    else :
      $item .= "<a itemprop='item' href='".$lien."'><span itemprop='name'>".$nom."</span></a>";
    endif;
    $item .= "<meta itemprop='position' content='".$pos."' />";
    $item .= "</li>";
    return $item;
  }
  
  // Fonction d'affichage du fil d'ariane
  function wpBreadcrumb( $echo = true ) {
  
    global $pos;
  
    if ( is_front_page() && is_home() ) :
      // Default homepage
      return;
  
    else :
      $breadcrumb = "<nav id='nav-breadcrumbs' role='navigation'>";
      $breadcrumb .= "<ol id='breadcrumbs' itemscope itemtype='http://schema.org/BreadcrumbList'>";
  
      // Page d'accueil
      $breadcrumb .= tplItem(home_url(), __("Inno3", "ihag"), "1", false, "home");
  
      if ( is_page() ) :
        // Traite la page et ses parents
        $ancestors = array_reverse( get_post_ancestors(get_the_ID()) );
        array_push($ancestors, get_the_ID());
        $count = count($ancestors);
        foreach ( $ancestors as $ancestor ) :
          $name = strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) );
  
          if ( $ancestor != end($ancestors) ) : // Si ancetre
            $breadcrumb .= tplItem(get_permalink($ancestor), $name, $pos);
  
          else : // Si page courante
            /* $breadcrumb .= tplItem(get_permalink($ancestor), $name, $pos, true); */
  
          endif;
          $pos++;
        endforeach;
        // endif is_page()
  
      elseif ( is_author() ) :
        $page = get_post(get_field("about_page", 'option'));
        $breadcrumb .= tplItem(get_permalink($page), get_the_title($page), $pos);
        $pos++;

        $page = get_post(get_field("team_page", 'option'));
        $breadcrumb .= tplItem(get_permalink($page), get_the_title($page), $pos);
        $pos++;

       /*  $author = get_user_by('slug', get_query_var('author_name'));
        $breadcrumb .= tplItem(get_author_posts_url($author->ID), $author->display_name, $pos, true); */
        // endif is_author()
      elseif ( is_archive() ) :
/*         if(is_post_type_archive('realisations')):
            $obj = get_post_type_object( 'realisations' );
            $breadcrumb .= tplItem(get_post_type_archive_link('realisations'), $obj->labels->name, $pos);
            $pos++;
        elseif(is_post_type_archive('projets')):
            $obj = get_post_type_object( 'projets' );
            $breadcrumb .= tplItem(get_post_type_archive_link('projets'), $obj->labels->name, $pos);
            $pos++;
        elseif(is_post_type_archive('news')):
            $obj = get_post_type_object( 'news' );
            $breadcrumb .= tplItem(get_post_type_archive_link('news'), $obj->labels->name, $pos);
            $pos++; */
        if((get_query_var('term'))):
          if (get_query_var('taxonomy') == 'blog-type') {
            $obj = get_post_type_object( 'news' );
            $breadcrumb .= tplItem(get_post_type_archive_link('news'), $obj->labels->name, $pos);
          }elseif (get_query_var('taxonomy') == 'real-type'){
            $obj = get_post_type_object( 'realisations' );
            $breadcrumb .= tplItem(get_post_type_archive_link('realisations'), $obj->labels->name, $pos);
          }
          $pos++;
          $breadcrumb .= nbParentTerms( get_query_var('term'), get_query_var('taxonomy'));
          $pos++;
        endif;
      
      // Page Post / Custom Post Type
      elseif ( is_single() ) :
  
        // On récupère le type du contenu
        $post_type = get_post_type_object( get_post_type() );
        
        if ( $post_type->name == "news"):
            $obj = get_post_type_object( 'news' );
            $breadcrumb .= tplItem(get_post_type_archive_link('news'), $obj->labels->name, $pos);
            $pos++;
            $taxo = wp_get_post_terms(get_the_ID(), 'blog-type');
            if ( !is_wp_error( $taxo ) && sizeof($taxo) > 0 ):
              $breadcrumb .= tplItem(get_term_link($taxo[0], "blog-type"), $taxo[0]->name, $pos);
              $pos++;
            endif;
        elseif ( $post_type->name == "realisations") : 
            $breadcrumb .= tplItem(get_post_type_archive_link($post_type->name), $post_type->label, $pos);
            $pos++;
            $taxo = wp_get_post_terms(get_the_ID(), 'real-type');
            if ( !is_wp_error( $taxo ) && sizeof($taxo) > 0 ) {
              $breadcrumb .= tplItem(get_term_link($taxo[0], "real-type"), $taxo[0]->name, $pos);
              $pos++;
            };


        elseif ( $post_type->name == "organisations") :
          if (wp_get_post_terms(get_the_ID(),'partners')){
            $page = get_field("partners_page", 'option');
            $breadcrumb .= tplItem(get_permalink($page),get_the_title($page), $pos);
            $pos++;
          }else if (wp_get_post_terms(get_the_ID(),'neighbor')){
            $page = get_field("neighbor_page", 'option');
            $breadcrumb .= tplItem(get_permalink($page),get_the_title($page), $pos);
            $pos++;
          }else if (wp_get_post_terms(get_the_ID(),'references')){
            $page = get_field("references_page", 'option');
            $breadcrumb .= tplItem(get_permalink($page),get_the_title($page), $pos);
            $pos++;
          };

        elseif ( $post_type->name == "projets") : 
            $page = get_field("projects_page", 'option');
            $breadcrumb .= tplItem(get_permalink($page), $post_type->label, $pos);
            $pos++;
            $taxo = wp_get_post_terms(get_the_ID(), 'project-type');
        endif;

  
        /* $breadcrumb .= tplItem(get_permalink(), get_the_title(), $pos, true); */
        // endif is_single()
  
      // Page Recherche
      elseif ( is_search() ) :
        $breadcrumb .= tplItem(get_permalink(), __("Résultats de votre recherche sur")." &laquo;".get_search_query()."&raquo;", $pos, true);
        // endif is_search()
  
  
      // Page 404
      elseif ( is_404() ) :
        $breadcrumb .= tplItem(get_permalink(), __("404 - Page non trouvée"), $pos, true);
        // endif is_404()
  
      
      endif;
  
      $breadcrumb .= "</ol>";
      $breadcrumb .= "</nav>";
  
      if ( $echo ) :
        echo $breadcrumb;
      else :
        return $breadcrumb;
      endif;
  
    endif;
  }
  
  // Affiche toutes les catégories parentes
  function nbParentCategories($cat, $is_category=false) {
    global $pos;
  
    $return = "";
  
    $archive = get_field("archive_post", "options");
    $return .= tplItem(get_permalink($archive), get_the_title($archive), $pos);
    $pos++;
  
    $all_cats = get_category_parents($cat, false, "/", true);
    $catslugs = array_filter(explode("/", $all_cats));
  
    if ( $catslugs ) : // si slug non vide
      $i = 1;
      $last_index = sizeof($catslugs);
      foreach ( $catslugs as $catslug ) :
  
        $last = false;
        if ( $is_category && $i == $last_index ) :
          $last = true;
        endif;
  
        $categ = get_category_by_slug($catslug);
        $return .= tplItem(get_category_link($categ->term_id), $categ->name, $pos, $last);
        $pos++;
        $i++;
      endforeach;
    endif;
    return $return;
  }
  
  
  // Affiche toutes les taxonomies parentes
  /*
  Dans les options admin on doit définir si l'affichage du PostType se fait avec la taxo
  et si la base est une archive ou une page (à définir)
  
  Selon PostType : hierachical, has_archive, slug rewrite
  Récupérer la page parente (slug)
  */
  function nbParentTerms($term_slug, $taxo) {
    global $pos;
    $term = get_term_by( "slug", $term_slug, $taxo);
    $return = tplItem(get_term_link($term->term_id), $term->name, $pos);
    $pos++;
  
    $all_terms = get_ancestors($term->term_id, $taxo);
    if ( $all_terms ) :
      foreach ( $all_terms as $one_term ) :
        $term = get_term_by( "id", $one_term, $taxo);
        $return = tplItem(get_term_link($term->term_id), $term->name, $pos).$return;
        $pos++;
      endforeach;
    endif;
  
    return $return;
  }
  
  // Affiche le lien vers page d'archive (pour un post-type)
  function nbParentArchive($cpt) {
    global $pos;
  
    $return = "";
    $obj = get_post_type_object( $cpt );
    $return .= tplItem(get_post_type_archive_link($cpt), $obj->labels->name, $pos);
    $pos++;
    return $return;
  }