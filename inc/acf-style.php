<?php
/**
 * Ihag_acf_styles
 *
 */

function ihag_register_acf_styles() {
	
	register_block_style(
		'core/button',
		array(
			'name'	        => 'bg-b',
			'label'	        => 'Fond bleu sans bordure'
		)
	); 
	register_block_style(
		'core/button',
		array(
			'name'	        => 'ol-b',
			'label'	        => 'Outline bleu sans fond'
		)
	); 
	register_block_style(
		'core/button',
		array(
			'name'	        => 'ol-o',
			'label'	        => 'Outline orange sans fond'
		)
	); 

	register_block_style(
		'core/cover',
		array(
			'name'	        => 'small-bottom-title',
			'label'	        => 'Arrière plan fin et titre bas'
		)
	); 




	register_block_style(
		'core/group',
		array(
			'name'	        => 'bg-big-left',
			'label'	        => 'Arrière plan grand gauche'
		)
	); 
	register_block_style(
		'core/group',
		array(
			'name'	        => 'bg-big-right',
			'label'	        => 'Arrière plan grand droite'
		)
	); 
	register_block_style(
		'core/group',
		array(
			'name'	        => 'bg-small-left',
			'label'	        => 'Arrière plan petit haut gauche'
		)
	); 
	register_block_style(
		'core/group',
		array(
			'name'	        => 'bg-small-right',
			'label'	        => 'Arrière plan petit haut droite'
		)
	); 
	register_block_style(
		'core/group',
		array(
			'name'	        => 'bg-contact',
			'label'	        => 'Arrière plan gauche contact'
		)
	); 


	
}

add_action( 'init', 'ihag_register_acf_styles' );
?>