// if(document.forms.namedItem("ihag-form")){
//     let form = document.forms.namedItem("ihag-form");
//     form.addEventListener('submit', function(e) {
//         e.preventDefault();
//         e.stopPropagation();
//         fetch(resturl + 'action-rest', {
//             method: 'POST',
//             body: new FormData(form),
//             headers: {'X-WP-Nonce': wpApiSettings.nonce},
//             cache: 'no-cache',
//         })
//         .then(
//             function(response) {
//                 if (response.status !== 200) { console.log(response.status); return;}
//                 response.json().then(function(data) {
//                     console.log(data);
//                 });
//             }
//         )
//         .catch(function(err) {
//             console.log('Fetch Error :-S', err);
//         });
//     });
// }

// TRAITEMENT ORGANISATIONS

if (document.getElementsByClassName('orga-filters').length >= 1) {
    let filters = Array.from(document.getElementsByClassName('post-tags-filters')[0].getElementsByTagName('button'))
    let organisations = Array.from(document.getElementsByClassName('orga-card-link'))
    filters.forEach(el => {
        el.addEventListener('click',function () {
            filters.forEach(e =>{
                e.classList.remove('active-button')
            })
            el.classList.add('active-button');
            organisations.forEach(orga => {
                if (el.classList[0] == orga.classList[0] || el.classList.contains('all')) {
                    setTimeout(() => {
                        orga.classList.add('active')
                        setTimeout(function () {
                            orga.classList.add('animate')
                        }, 200);
                    }, 200);
                }else {
                    orga.classList.remove('animate')
                    setTimeout(function () {
                        orga.classList.remove('active')
                    }, 200);
                }
            });
        })
    });
}

// ACTIVE ARCHIVE MENU

let bodyClasses = document.body.classList;
if (bodyClasses.contains('archive')) {
    if (bodyClasses.contains('post-type-archive-realisations')) {
        // Archive realisation
        document.getElementsByClassName('wp-block-navigation-item menu-real')[0].classList.add('current-menu-item')
    }else if(bodyClasses.contains('post-type-archive-news')){
        // Archive news
        document.getElementsByClassName('wp-block-navigation-item menu-news')[0].classList.add('current-menu-item')
    }else if (bodyClasses.contains('term-file')) {
        // Taxonomie dossier
        document.getElementsByClassName('wp-block-navigation-item menu-real-file')[0].classList.add('current-menu-item')
    }else if (bodyClasses.contains('term-tool')) {
        // Taxonomie outils
        document.getElementsByClassName('wp-block-navigation-item menu-real-tool')[0].classList.add('current-menu-item')
    }else if (bodyClasses.contains('term-publication')) {
        // Taxonomoe publication
        document.getElementsByClassName('wp-block-navigation-item menu-real-publication')[0].classList.add('current-menu-item')
    }else if (bodyClasses.contains('term-article')) {
        // Taxonomie article
        document.getElementsByClassName('wp-block-navigation-item menu-news-article')[0].classList.add('current-menu-item')
    }else if (bodyClasses.contains('term-event')) {
        // Taxonomie evenement
        document.getElementsByClassName('wp-block-navigation-item menu-news-event')[0].classList.add('current-menu-item')
    }else if (bodyClasses.contains('term-jobs')) {
        // Taxonomie recrutement
        document.getElementsByClassName('wp-block-navigation-item menu-news-jobs')[0].classList.add('current-menu-item')
    }
}