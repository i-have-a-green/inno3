<?php
/**
 * Title: Page de détail réalisation timeline
 * Slug: ihag/single-timeline-real
 * Categories:  realisation
 */
?>
<!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
<main class="wp-block-group alignfull site-main"><!-- wp:acf/hero-real {"id":"block_62bb13e70b971","name":"acf/hero-real","align":"wide","mode":"preview"} /-->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%"><!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%"><!-- wp:group {"style":{"spacing":{"padding":{"left":"2rem"}}},"className":"timeline"} -->
<div class="wp-block-group timeline" style="padding-left:2rem"><!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2001</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Entrée dans le monde de l'Open Source</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Premières expériences et accompagnements entourant les licences libres, au profit de projets communautaires et de créateurs indépendants. Graduellement, l'activité s'est étendue à l'accompagnement de projets d'entreprises désirant adopter une stratégie de diffusion Open Source de leur(s) logiciel(s). Cette période s'est notamment traduite par la fondation de l'Association Veni, Vidi, Libri en 2006 (vulgarisation et diffusion de connaissances relatives aux licences libres).</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2007</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Premières expériences industrielles</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>De 2007 à 2011, ce fut l'occasion de confronter les logiques communautaires, collaboratives et contractuelles des communautés avec celles pragmatiques des entreprises du secteur. Cette période fut aussi celle de l'innovation avec la formalisation pour la première fois d'une série de modules de formations dédiés à la maîtrise des risques juridiques liés à l'Open Source, à l'organisation des collaborations autour de projets ouverts et à l'utilisation des principaux outils d'audit de code, et au lancement du Centre Juridique de l'Open Source.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2011</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Création du cabinet</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Aller au delà des aspects purement juridiques de la stratégie Open Source, en associant une diversité de compétences complémentaires.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2013</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Ouverture des bureaux parisiens et thématiques métiers</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>inno³ s'agrandit et de nouvelles offres apparaissent dans le domaine du consulting et du service aux entreprises. Développement de l'activité d'inno³ au travers de partenariats « orientés métier » dans le domaine de l'éducation, des transports et de la géomatique.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2014</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Expansion sous toutes ses formes</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Lancement des inno'vents et de l'inno'thèque pour maximiser le partage de nos réflexions et pratiques.<br>Des moments très prolifiques, riches en rencontres et moments de convivialités.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2016</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Ouverture du 137</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Une nouvelle année plein de paris : lancement du 137 dans une démarche plus communautaire (nouveaux locaux de 250 m²), la coprésidence de l'Open Source Summit 2016 et deux nouvelles recrues.<br>Présence forte auprès du partenariat pour un gouvernement ouvert dans le cadre de la présidence de l'Open Source Summit.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2019</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Développement de l'activité scientifique</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Formalisation d'une activité scientifique reconnue sur la science ouverte et les communs numériques avec l'arrivée de Célya et Vincent.<br>Consolidation des outils open source et de nos méthodes d'animation en ligne (#Covid). Lancement des Rencontres Open ESR et du partenariat avec le COSO.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"style":"solid","width":"2px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","backgroundColor":"color__white","layout":{"inherit":false}} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">2021</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Design et soutenabilité dans nos pratiques</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Développement d'une démarche Design avec l'arrivée de Romain, et une implication plus forte sur la soutenabilité des projets ouvert, notamment avec le lancement du projet Hermine mené par Camille.<br>L'occasion également de dressé un bilan de nos activitées en marquant nos 10 années de pratiques.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px","radius":"0px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"textColor":"primary_rust","layout":{"wideSize":"","inherit":false}} -->
<div class="wp-block-group has-primary-rust-color has-text-color" style="border-radius:0px;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_rust"} -->
<h3 class="has-primary-rust-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px","radius":"0px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"textColor":"primary_rust","layout":{"wideSize":"","inherit":false}} -->
<div class="wp-block-group has-primary-rust-color has-text-color" style="border-radius:0px;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_rust"} -->
<h3 class="has-primary-rust-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

</main>
<!-- /wp:group -->