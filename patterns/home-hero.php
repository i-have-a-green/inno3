<?php
/**
 * Title: Entête home avec titre custom
 * Slug: ihag/hero-home
 * Categories:  home
 */
?>
    <!-- wp:cover {"url":"http://inno3.local/wp-content/uploads/2022/06/Image_ENTETE-V3.svg","id":181,"dimRatio":0,"focalPoint":{"x":"0.50","y":"0.46"},"contentPosition":"bottom center","isDark":false,"align":"full","className":"hero-home"} -->
    <div class="wp-block-cover alignfull is-light has-custom-content-position is-position-bottom-center hero-home"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-181" alt="" src="http://inno3.local/wp-content/uploads/2022/06/Image_ENTETE-V3.svg" style="object-position:50% 46%" data-object-fit="cover" data-object-position="50% 46%"/><div class="wp-block-cover__inner-container"><!-- wp:heading {"textAlign":"left","level":1,"textColor":"primary_turquoise","className":"alignwide"} -->
    <h1 class="has-text-align-left alignwide has-primary-turquoise-color has-text-color">Innovation<br><em>is <strong>openness</strong></em></h1>
    <!-- /wp:heading --></div></div>
    <!-- /wp:cover -->