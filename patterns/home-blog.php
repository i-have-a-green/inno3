<?php
/**
 * Title: Liste article blog home
 * Slug: ihag/home-blog
 * Categories:  home
 */
?>
<!-- wp:group {"align":"wide","className":"is-style-bg-big-left"} -->
<div class="wp-block-group alignwide is-style-bg-big-left"><!-- wp:columns {"align":"wide"} -->
        <div class="wp-block-columns alignwide"><!-- wp:column {"width":"66.66%"} -->
        <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:heading -->
        <h2>Dernières actualités</h2>
        <!-- /wp:heading --></div>
        <!-- /wp:column -->
        
        <!-- wp:column {"width":"33.33%"} -->
        <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
        <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
        <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link">Toutes notre actualité</a></div>
        <!-- /wp:button --></div>
        <!-- /wp:buttons --></div>
        <!-- /wp:column --></div>
        <!-- /wp:columns -->
        
        <!-- wp:acf/bloc-listing-blog-home {"id":"block_62a0614de9ddc","name":"acf/bloc-listing-blog-home","align":"wide","mode":"preview"} /--></div>
        <!-- /wp:group -->