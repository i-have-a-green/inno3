<?php
/**
 * Title: Détail des articles contenu libre
 * Slug: ihag/single-blog
 * Categories:  blog
 */
?>
<!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
<main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
<div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"35%","layout":{"inherit":true}} -->
<div class="wp-block-column" style="flex-basis:35%"><!-- wp:post-featured-image /--></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b198cc854e8","name":"acf/breadcrumb","align":"","mode":"preview"} /-->

<!-- wp:post-title {"level":1} /-->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"bottom","width":"66.66%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:66.66%"><!-- wp:post-date /--></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} /--></div>
<!-- /wp:buttons --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0rem","right":"0rem","bottom":"0rem","left":"0rem"}},"border":{"width":"0px","style":"none"}}} -->
<div class="wp-block-group" style="border-style:none;border-width:0px;padding-top:0rem;padding-right:0rem;padding-bottom:0rem;padding-left:0rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Auteur/Autrice</h3>
<!-- /wp:heading -->

<!-- wp:acf/bloc-contributeurs {"id":"block_62c3fc636da99","name":"acf/bloc-contributeurs","data":{"contributors_0_contributor":2,"_contributors_0_contributor":"field_62c3fbfc2ba48","contributors_1_contributor":1,"_contributors_1_contributor":"field_62c3fbfc2ba48","contributors":2,"_contributors":"field_62c3fbed2ba47"},"align":"","mode":"preview"} /--></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} /-->

<!-- wp:button {"className":"is-style-ol-b"} /-->

<!-- wp:button {"className":"is-style-ol-b"} /--></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></main>
<!-- /wp:group -->