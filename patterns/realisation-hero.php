<?php
/**
 * Title: Entête de la page réalisation
 * Slug: ihag/hero-real
 * Categories:  realisation
 */
?>
    <!-- wp:columns {"align":"wide"} -->
    <div class="wp-block-columns alignwide"><!-- wp:column {"width":"66.66%"} -->
    <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph {"placeholder":"Paragraphe de texte de la page blog. Contenu à renseigner"} -->
    <p></p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:column -->
    
    <!-- wp:column {"width":"33.33%"} -->
    <div class="wp-block-column" style="flex-basis:33.33%">
    <!-- wp:heading {"level":3} -->
    <h3></h3>
    <!-- /wp:heading -->
    
    <!-- wp:paragraph -->
    <p></p>
    <!-- /wp:paragraph -->
    
    <!-- wp:buttons -->
    <div class="wp-block-buttons"><!-- wp:button /--></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns -->