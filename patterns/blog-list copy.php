<?php
/**
 * Title: En savoir plus 1
 * Slug: ihag/know-more
 * Categories:  global
 */
?>
<!-- wp:group {"align":"wide","className":"is-style-bg-small-left"} -->
<div class="wp-block-group alignwide is-style-bg-small-left"><!-- wp:heading -->
<h2>Pour en savoir plus<br>sur Inno<sup>3</sup></h2>
<!-- /wp:heading -->

<!-- wp:columns {"align":"full","style":{"spacing":{"blockGap":"2rem"}}} -->
<div class="wp-block-columns alignfull"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px","style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"backgroundColor":"color__white"} -->
<div class="wp-block-group has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3} -->
<h3>Notre équipe</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Découvrez nos ressources mise à disposition en license ouverte et les outils libres que nous employons et valorisons</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>N'hésitez pas à témoigner d'une utilisation, signaler un besoin ou une anomalie, ou directement contribuer à l'enrichissement de ses supports dans une logique collaborative</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"border":{"radius":"0px"}},"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link" href="https://innocube.ihaveagreen.fr/a-propos/lequipe/" style="border-radius:0px">Découvrir notre équipe</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"top"} -->
<div class="wp-block-column is-vertically-aligned-top"><!-- wp:group {"style":{"border":{"width":"2px","style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"backgroundColor":"color__white","layout":{"inherit":true}} -->
<div class="wp-block-group has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3} -->
<h3>Le 137</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Découvrez nos ressources mise à disposition en license ouverte et les outils libres que nous employons et valorisons</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>N'hésitez pas à témoigner d'une utilisation, signaler un besoin ou une anomalie, ou directement contribuer à l'enrichissement de ses supports dans une logique collaborative</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"border":{"radius":"0px"}},"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link" href="https://innocube.ihaveagreen.fr/a-propos/le-137/" style="border-radius:0px">Découvrir le 137</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px","style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"backgroundColor":"color__white"} -->
<div class="wp-block-group has-color-white-background-color has-background" style="border-style:solid;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3} -->
<h3>Nos partenaires</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Découvrez nos ressources mise à disposition en license ouverte et les outils libres que nous employons et valorisons</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>N'hésitez pas à témoigner d'une utilisation, signaler un besoin ou une anomalie, ou directement contribuer à l'enrichissement de ses supports dans une logique collaborative</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"border":{"radius":"0px"}},"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link" href="https://innocube.ihaveagreen.fr/a-propos/nos-partenaires/" style="border-radius:0px">Découvrir nos partenaires</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->