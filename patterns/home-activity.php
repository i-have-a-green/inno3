<?php
/**
 * Title: Bloc activité
 * Slug: ihag/home-activity
 * Categories:  home
 */
?>
    <!-- wp:group {"align":"wide","className":"is-style-bg-small-left","layout":{"inherit":true}} -->
    <div class="wp-block-group alignwide is-style-bg-small-left"><!-- wp:heading {"align":"wide"} -->
    <h2 class="alignwide">Nos activités</h2>
    <!-- /wp:heading -->

    <!-- wp:columns -->
    <div class="wp-block-columns"><!-- wp:column -->
    <div class="wp-block-column"><!-- wp:paragraph -->
    <p>Nous proposons trois type d\'activités mobilisant différentes expertises complémentaires, multisectorielles et interdisciciplinaires.</p>
    <!-- /wp:paragraph -->

    <!-- wp:paragraph -->
    <p>Ces activités témoignent des compétences de notre équipe de la gestion de la propriété intellectuelle, à la mise en conformité, en passant par la gestion de communauté et de communs numérique.</p>
    <!-- /wp:paragraph -->

    <!-- wp:paragraph -->
    <p>Nous apportons une expertise en droit, sociologie, méthodes numérique et design</p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right","orientation":"vertical"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} -->
    <div class="wp-block-button is-style-bg-b"><a class="wp-block-button__link">Détails de nos activités</a></div>
    <!-- /wp:button -->

    <!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link">L\'équipe Inno<sup>3</sup></a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns -->

    <!-- wp:spacer {"height":"24px"} -->
    <div style="height:24px" aria-hidden="true" class="wp-block-spacer"></div>
    <!-- /wp:spacer -->

    <!-- wp:columns {"align":"wide"} -->
    <div class="wp-block-columns alignwide"><!-- wp:column -->
    <div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"2rem","right":"2rem","bottom":"2rem","left":"2rem"}}},"borderColor":"primary_turquoise","className":"home-card"} -->
    <div class="wp-block-group home-card has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:2rem;padding-right:2rem;padding-bottom:2rem;padding-left:2rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
    <h3 class="has-primary-turquoise-color has-text-color">Conseil</h3>
    <!-- /wp:heading -->

    <!-- wp:paragraph {"textColor":"primary_turquoise"} -->
    <p class="has-primary-turquoise-color has-text-color">Avec notre approche unique et personnalisée, nous définissons avec nos clients et partenaires des stratégies d’ouverture ambitieuses et des politiques pragmatiques pour les mettre en œuvre.</p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"2rem","right":"2rem","bottom":"2rem","left":"2rem"}}},"borderColor":"primary_turquoise","className":"home-card"} -->
    <div class="wp-block-group home-card has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:2rem;padding-right:2rem;padding-bottom:2rem;padding-left:2rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
    <h3 class="has-primary-turquoise-color has-text-color">Recherche</h3>
    <!-- /wp:heading -->

    <!-- wp:paragraph {"textColor":"primary_turquoise"} -->
    <p class="has-primary-turquoise-color has-text-color">Dans une démarche de recherche-action constante, nous nous définissons comme un collectif de chercheur.se.s alliant méthodes mixtes et numérique associant réfléxivité et éthiques à nos traveaux.</p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"2rem","right":"2rem","bottom":"2rem","left":"2rem"}}},"borderColor":"primary_turquoise","className":"home-card"} -->
    <div class="wp-block-group home-card has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:2rem;padding-right:2rem;padding-bottom:2rem;padding-left:2rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
    <h3 class="has-primary-turquoise-color has-text-color">Formation</h3>
    <!-- /wp:heading -->

    <!-- wp:paragraph {"textColor":"primary_turquoise"} -->
    <p class="has-primary-turquoise-color has-text-color">Centre de formation certifié Qualliopi, nous assurons par ailleurs une veille constantes afin d\'assurer un transfert de compétences optimal dans le cadre de nos offre de formation dédiée</p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns --></div>
    <!-- /wp:group -->
