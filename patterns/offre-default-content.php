<?php
/**
 * Title: Page parent Offre par défaut
 * Slug: ihag/offer-default
 * Categories:  offer
 */
?>
    <!-- wp:cover {"url":"http://inno3.local/wp-content/uploads/2022/06/Image_ENTETE-V3.svg","id":181,"dimRatio":0,"isDark":false,"align":"full","className":"is-style-small-bottom-title"} -->
    <div class="wp-block-cover alignfull is-light is-style-small-bottom-title"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-181" alt="" src="http://inno3.local/wp-content/uploads/2022/06/Image_ENTETE-V3.svg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:acf/breadcrumb {"id":"block_62b2d28c005ee","name":"acf/breadcrumb","data":{},"align":"","mode":"preview"} /-->
    
    <!-- wp:heading {"level":1,"textColor":"primary_turquoise"} -->
    <h1 class="has-primary-turquoise-color has-text-color">Notre offre</h1>
    <!-- /wp:heading --></div></div>
    <!-- /wp:cover -->
    
    <!-- wp:paragraph -->
    <p>Nous proposons trois type d\'activités mobilisant différentes expertises complémentaires, multisectoriel et interdisciciplinaire. Ces activités témoignent des compétences de notre équipe de la gestion de la propriété intellectuelle, à la mise en conformité, en passant par la gestion de communauté et de communs numérique. Nous apportons une expertise en droit, sociologie, méthodes numérique et design</p>
    <!-- /wp:paragraph -->
    
    <!-- wp:columns {"align":"wide"} -->
    <div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"top"} -->
    <div class="wp-block-column is-vertically-aligned-top"><!-- wp:group {"style":{"border":{"style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","layout":{"inherit":true}} -->
    <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3} -->
    <h3>Cabinet de conseils</h3>
    <!-- /wp:heading -->
    
    <!-- wp:list -->
    <ul><li>Auditer et valoriser vos projet</li><li>Transformer votre organisation</li><li>Définir et organiser une démarche communautaire</li></ul>
    <!-- /wp:list -->
    
    <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link" href="http://inno3.local/activite/offre/conseil/">Notre offre de conseil</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column -->
    
    <!-- wp:column {"verticalAlignment":"top"} -->
    <div class="wp-block-column is-vertically-aligned-top"><!-- wp:group {"style":{"border":{"style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","layout":{"inherit":true}} -->
    <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3} -->
    <h3>Collectif de chercheurs</h3>
    <!-- /wp:heading -->
    
    <!-- wp:list -->
    <ul><li>Recherche-action</li><li>Méthodes mixtes et numérique</li><li>Démarche de science ouverte et réflexivité éthique</li></ul>
    <!-- /wp:list -->
    
    <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link" href="http://inno3.local/activite/offre/conseil/">Notre offre de recherche</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column -->
    
    <!-- wp:column {"verticalAlignment":"top"} -->
    <div class="wp-block-column is-vertically-aligned-top"><!-- wp:group {"style":{"border":{"style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise","layout":{"inherit":true}} -->
    <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3} -->
    <h3>Centre de formation</h3>
    <!-- /wp:heading -->
    
    <!-- wp:list -->
    <ul><li>Auditer et valoriser vos projet</li><li>Transformer votre organisation</li><li>Définir et organiser une démarche communautaire</li></ul>
    <!-- /wp:list -->
    
    <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link" href="http://inno3.local/activite/offre/conseil/">Notre offre de formation</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns -->
    
    <!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"4rem","bottom":"4rem"}}},"backgroundColor":"primary_turquoise"} -->
    <div class="wp-block-group alignfull has-primary-turquoise-background-color has-background" style="padding-top:4rem;padding-bottom:4rem"><!-- wp:columns {"verticalAlignment":null,"className":"alignwide"} -->
    <div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center","width":"33.33%","layout":{"inherit":false}} -->
    <div class="wp-block-column is-vertically-aligned-center" style="flex-basis:33.33%"><!-- wp:heading {"textColor":"color__white"} -->
    <h2 class="has-color-white-color has-text-color">Pour en <br>savoir plus sur nos activités</h2>
    <!-- /wp:heading --></div>
    <!-- /wp:column -->
    
    <!-- wp:column {"width":"66.66%"} -->
    <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:columns -->
    <div class="wp-block-columns"><!-- wp:column -->
    <div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}},"border":{"style":"solid"}},"borderColor":"color__white"} -->
    <div class="wp-block-group has-border-color has-color-white-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"color__white"} -->
    <h3 class="has-color-white-color has-text-color">Nos projets</h3>
    <!-- /wp:heading -->
    
    <!-- wp:paragraph {"textColor":"color__white"} -->
    <p class="has-color-white-color has-text-color">Marshall, Will, and Holly on a routine expedition, met the greatest earthquake ever known. High on the rapids, it struck their tiny raft! And plunged them down a thousand feet below… to the Land of the Lost! Lost! Lost! Lost! Lost!</p>
    <!-- /wp:paragraph -->
    
    <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link">Parcourir nos projets</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column -->
    
    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}},"border":{"style":"solid"}},"borderColor":"color__white"} -->
    <div class="wp-block-group has-border-color has-color-white-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"color__white"} -->
    <h3 class="has-color-white-color has-text-color">Nos références</h3>
    <!-- /wp:heading -->
    
    <!-- wp:paragraph {"textColor":"color__white"} -->
    <p class="has-color-white-color has-text-color">Marshall, Will, and Holly on a routine expedition, met the greatest earthquake ever known. High on the rapids, it struck their tiny raft! And plunged them down a thousand feet below… to the Land of the Lost! Lost! Lost! Lost! Lost!</p>
    <!-- /wp:paragraph -->
    
    <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link">Parcourir nos références</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:group --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns --></div>
    <!-- /wp:group -->