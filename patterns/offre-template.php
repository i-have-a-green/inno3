<?php
/**
 * Title: Page de détail d'offre
 * Slug: ihag/offer-template
 * Categories:  offer  
 */
?>
<!-- wp:cover {"url":"http://inno3.local/wp-content/uploads/2022/06/Image_ENTETE-V3.svg","id":181,"dimRatio":0,"minHeight":173,"minHeightUnit":"px","isDark":false,"align":"full","className":"is-style-small-bottom-title"} -->
<div class="wp-block-cover alignfull is-light is-style-small-bottom-title" style="min-height:173px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-181" alt="" src="http://inno3.local/wp-content/uploads/2022/06/Image_ENTETE-V3.svg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:acf/breadcrumb {"id":"block_62b2d10ce265d","name":"acf/breadcrumb","data":{},"align":"","mode":"preview"} /-->

<!-- wp:heading {"level":1} -->
<h1></h1>
<!-- /wp:heading --></div></div>
<!-- /wp:cover -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:heading -->
<h2></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}},"border":{"style":"solid"}},"borderColor":"primary_turquoise","textColor":"primary_turquoise"} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color has-primary-turquoise-color has-text-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}},"border":{"style":"solid"}},"borderColor":"primary_turquoise"} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"primary_turquoise"} -->
<p class="has-primary-turquoise-color has-text-color"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->


<!-- wp:group {"align":"wide","className":"is-style-bg-big-left"} -->
<div class="wp-block-group alignwide is-style-bg-big-left"><!-- wp:heading {"level":3} -->
<h3>Découvrir les projets en lien avec cette activité</h3>
<!-- /wp:heading -->

<!-- wp:acf/bloc-listing-projet-sur-offre {"id":"block_62b2ecdb2fd75","name":"acf/bloc-listing-projet-sur-offre","align":"","mode":"preview"} /--></div>
<!-- /wp:group -->