<?php
/**
 * Title: Entête page listing blog
 * Slug: ihag/hero-blog
 * Categories:  blog
 */
?>

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph {"placeholder":"Paragraphe de texte de la page blog. Contenu à renseigner"} -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:acf/bloc-random-post {"id":"block_62972b6327797","name":"acf/bloc-random-post","align":"","mode":"preview"} /--></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->