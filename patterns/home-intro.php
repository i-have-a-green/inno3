<?php
/**
 * Title: Intro avec CTA page home
 * Slug: ihag/home-intro
 * Categories:  home
 */
?>
    <!-- wp:columns -->
    <div class="wp-block-columns"><!-- wp:column -->
    <div class="wp-block-column"><!-- wp:heading {"level":3} -->
    <h3>Inno<sup>3</sup> est un cabinet de conseil spécialiste des modèles ouverts.</h3>
    <!-- /wp:heading -->
    
    <!-- wp:buttons -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} -->
    <div class="wp-block-button is-style-bg-b"><a class="wp-block-button__link">Le cabinet</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:column -->
    
    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:paragraph -->
    <p>Avec notre approche unique et personnalisée, nous définissons avec nos clients et partenaires des stratégies d’ouverture ambitieuses et des politiques pragmatiques pour les mettre en œuvre.</p>
    <!-- /wp:paragraph -->
    
    <!-- wp:paragraph -->
    <p>Participant ainsi à la construction et diffusion des meilleures pratiques en matière d’ouverture et collaboration, notre équipe est à l’origine et s’implique continuellement dans les projets structurants de cette dynamique.</p>
    <!-- /wp:paragraph -->
    
    <!-- wp:paragraph -->
    <p>Les modèles ouverts incluent : open source, open data, open source hardware, communs numériques, innovation ouverte et science ouverte.</p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns -->