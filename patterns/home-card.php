<?php
/**
 * Title: Liste 3 cartes avec visuel
 * Slug: ihag/home-card
 * Categories:  home
 */
?>
    <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"2rem","right":"2rem","bottom":"2rem","left":"2rem"}}},"borderColor":"primary_turquoise","className":"home-card"} -->
    <div class="wp-block-group home-card has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:2rem;padding-right:2rem;padding-bottom:2rem;padding-left:2rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
    <h3 class="has-primary-turquoise-color has-text-color"></h3>
    <!-- /wp:heading -->
    
    <!-- wp:paragraph {"textColor":"primary_turquoise"} -->
    <p class="has-primary-turquoise-color has-text-color"></p>
    <!-- /wp:paragraph --></div>
    <!-- /wp:group -->
