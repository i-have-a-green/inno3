<?php
/**
 * Title: Page de détail réalisation
 * Slug: ihag/single-real
 * Categories:  realisation
 */
?>
<!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
<main class="wp-block-group alignfull site-main"><!-- wp:acf/hero-real {"id":"block_62bb13e70b971","name":"acf/hero-real","align":"wide","mode":"preview"} /-->

<!-- wp:buttons {"className":"real-absolute-btn","layout":{"type":"flex","justifyContent":"right"}} -->
<div class="wp-block-buttons real-absolute-btn"><!-- wp:button {"className":"is-style-bg-b"} -->
<div class="wp-block-button is-style-bg-b"><a class="wp-block-button__link">A remplir</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Organisations</h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:acf/bloc-organisations {"id":"block_62c54d315a683","name":"acf/bloc-organisations","data":{"field_62c5498285f19":""},"align":"","mode":"preview"} /--></div>
<!-- /wp:group -->

<!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
<h3 class="has-primary-turquoise-color has-text-color">Auteur</h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:acf/bloc-contributeurs {"id":"block_62c54dda48797","name":"acf/bloc-contributeurs","data":{"field_62c3fbed2ba47":""},"align":"","mode":"preview"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></main>
<!-- /wp:group -->