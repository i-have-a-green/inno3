<?php
/**
 * Title: Liste des réalisations page Home
 * Slug: ihag/home-real
 * Categories:  home
 */
?>
    <!-- wp:group {"align":"wide","className":"is-style-bg-big-right"} -->
    <div class="wp-block-group alignwide is-style-bg-big-right"><!-- wp:columns {"align":"wide"} -->
    <div class="wp-block-columns alignwide"><!-- wp:column {"width":"66.66%"} -->
    <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:heading -->
    <h2>Dernières réalisations</h2>
    <!-- /wp:heading --></div>
    <!-- /wp:column -->
    
    <!-- wp:column {"width":"33.33%"} -->
    <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right"}} -->
    <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} -->
    <div class="wp-block-button is-style-ol-b"><a class="wp-block-button__link">Toutes nos réalisations</a></div>
    <!-- /wp:button --></div>
    <!-- /wp:buttons --></div>
    <!-- /wp:column --></div>
    <!-- /wp:columns -->
    
    <!-- wp:acf/bloc-listing-realisations-home {"id":"block_62a05fa7e52c0","name":"acf/bloc-listing-realisations-home","align":"wide","mode":"preview"} /--></div>
    <!-- /wp:group -->