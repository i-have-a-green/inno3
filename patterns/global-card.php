<?php
/**
 * Title: Carte encadrée global orange
 * Slug: ihag/global-card
 * Categories:  global
 */
?>

<!-- wp:group {"style":{"border":{"width":"2px","radius":"0px"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"textColor":"primary_rust","layout":{"wideSize":"","inherit":false}} -->
<div class="wp-block-group has-primary-rust-color has-text-color" style="border-radius:0px;border-width:2px;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:heading {"level":3,"textColor":"primary_rust"} -->
<h3 class="has-primary-rust-color has-text-color"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:group -->