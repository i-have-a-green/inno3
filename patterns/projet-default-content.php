<?php
/**
 * Title: Page de détail des projets
 * Slug: ihag/projet-template
 * Categories:  project
 */
?>
	<!-- wp:columns {"align":"wide","className":"single-header"} -->
<div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"20%","layout":{"inherit":true}} -->
<div class="wp-block-column" style="flex-basis:20%"><!-- wp:post-featured-image /--></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b198cc854e8","name":"acf/breadcrumb","align":"","mode":"preview"} /-->

<!-- wp:post-title {"level":1} /--></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:group {"style":{"border":{"style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise"} -->
<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:paragraph {"textColor":"primary_turquoise"} -->
<p class="has-primary-turquoise-color has-text-color"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->