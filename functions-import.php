<?php

// ajout de l'option dans le submenu
/* add_action('admin_menu', 'ihag_user_sub_menu');
function ihag_user_sub_menu() {
    add_submenu_page(
        'tools.php',
        'toolsRedactor',
        'Importer CSV',
        'manage_options',
        'toolsRedactor',
        'toolsRedactorBtn',
    );
    global $submenu;
} */


// gestion csv
function toolsRedactorBtn(){
    ?>
    <div class="wrap">
        <h1>Importer des actualités</h1>
        <a href="?report=importArticle" class="button">Importer des actualités</a>
    </div>
    <div class="wrap">
        <h1>Importer des réalisations</h1>
        <a href="?report=importReal" class="button">Importer des réalisations</a>
    </div>
    <div class="wrap">
        <h1>Importer des projets</h1>
        <a href="?report=importProject" class="button">Importer des projets</a>
    </div>
    <div class="wrap">
        <h1>Importer des écosystèmes</h1>
        <a href="?report=importEcosystem" class="button">Importer des écosystèmes</a>
    </div>
    <div class="wrap">
        <h1>Importer des tags</h1>
        <a href="?report=importTags" class="button">Importer des tags</a>
    </div>
    <?php
}
add_action('init', function() {
    if (isset($_GET['report']) && $_GET['report'] == 'importArticle'){
        clearOldPost('news');
        importActualityArticle();
        importActualityEvent();
        importActualityJobs();
    } else if (isset($_GET['report']) && $_GET['report'] == 'importReal'){
        clearOldPost('realisations');
        importRealPublication();
        importRealTool();
    } else if (isset($_GET['report']) && $_GET['report'] == 'importEcosystem'){
        clearOldPost('organisations');
        importEcoResident();
        importEcoPartner();
        importEcoReference();
    } else if (isset($_GET['report']) && $_GET['report'] == 'importProject'){
        clearOldPost('projets');
        importProject();
    } else if (isset($_GET['report']) && $_GET['report'] == 'importTags'){
        clearOldTags();
        importTags();
    }
});
function clearOldPost($type){
    $oldPosts= get_posts( array('post_type'=>$type, 'numberposts'=>-1) );
    foreach ($oldPosts as $oldPost) {
        wp_delete_attachment( get_post_thumbnail_id ($oldPost->ID ), true );
        wp_delete_post( $oldPost->ID, true );
    };
}
function clearOldTags(){
    $oldTags= get_terms( array(
        'taxonomy' => 'global_tags',
        'hide_empty' => false) );
    foreach ($oldTags as $oldTag) {
        wp_delete_term( $oldTag->term_id, 'global_tags' );
    };
}

if ( ! function_exists( 'media_handle_upload' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );
}

function import_image( $url, $post_id ) {
    if ($url != '') {
        $tmp = download_url( $url );
        $file_array = array();
    
        preg_match( '/[^\?]+\.(jpg|jpe|jpeg|gif|png|pdf)/i', $url, $matches );
        $file_array['name'] = @basename( $matches[0] );
        $file_array['tmp_name'] = $tmp;
    
        if ( is_wp_error( $tmp ) ) {
            @unlink( $file_array['tmp_name'] );
            $file_array['tmp_name'] = '';
        }
    
        $id = media_handle_sideload( $file_array, $post_id );
    
        if ( is_wp_error( $id ) ) {
            @unlink( $file_array['tmp_name'] );
            return $id;
        }
        update_post_meta( $id, 'old_url', $url );
    
        return $id;
    } 
}

//news
function importActualityArticle(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/news-article.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            // Formatage date
            $sepHour = explode(' ', $data[7]);
            $sepHourTemp = array();
            foreach ($sepHour as $key => $value) {
                if (!empty($value)) {
                    $sepHourTemp[] = $sepHour[$key];
                }
            }
            $sepHour = $sepHourTemp;
            $rawDate = $data[6] . ' ' . $sepHour[1];


            $importPost = array(
                "post_title"    => $data[0],
                'post_date'     => $rawDate,
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"35%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:35%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b198cc854e8","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /-->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"bottom","width":"66.66%"} -->
                <div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:66.66%"><!-- wp:post-date /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons -->
                <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} /--></div>
                <!-- /wp:buttons --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>'. $data[9].$data[10] .'</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color"></h3>
                <!-- /wp:heading -->
                
                <!-- wp:group {"style":{"spacing":{"padding":{"top":"0rem","right":"0rem","bottom":"0rem","left":"0rem"}},"border":{"width":"0px","style":"none"}}} -->
                <div class="wp-block-group" style="border-style:none;border-width:0px;padding-top:0rem;padding-right:0rem;padding-bottom:0rem;padding-left:0rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Auteur/Autrice</h3>
                <!-- /wp:heading -->
                
                <!-- wp:acf/bloc-contributeurs {"id":"block_62c3fc636da99","name":"acf/bloc-contributeurs","data":{},"align":"","mode":"preview"} /--></div>
                <!-- /wp:group -->
                
                <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
                <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Ressources associées</h3>
                <!-- /wp:heading -->

                <!-- wp:list {"textColor":"primary_turquoise"}-->
                <ul><li class="has-primary-turquoise-color has-text-color">'. $data[11] .'</li></ul>
                <!-- /wp:list --></div>
                <!-- /wp:group --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></main>
                <!-- /wp:group -->'
                ,
                "post_type"     => "news",
                "tax_input"     => array("blog-type"    => array( "article" ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            if ( isset($data[5]) ) {
                // On regarde si l’image existe, sinon on l’insère
                if ( true ) {
                    $id_attachment = import_image( $data[5], $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

function importActualityEvent(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/news-event.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[13]);

            // Formatage date
            $sepHour = explode(' ', $data[6]);
            $sepHourTemp = array();
            foreach ($sepHour as $key => $value) {
                if (!empty($value)) {
                    $sepHourTemp[] = $sepHour[$key];
                }
            }
            $sepHour = $sepHourTemp;
            $rawDate = $data[5] . ' ' . $sepHour[1];


            $importPost = array(
                "post_title"    => $data[0],
                'post_date'     => $rawDate,
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"35%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:35%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b198cc854e8","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /-->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"bottom","width":"66.66%"} -->
                <div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:66.66%"><!-- wp:post-date /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons -->
                <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} /--></div>
                <!-- /wp:buttons --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>'. $data[7].$data[8] .'</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color"></h3>
                <!-- /wp:heading -->
                
                <!-- wp:group {"style":{"spacing":{"padding":{"top":"0rem","right":"0rem","bottom":"0rem","left":"0rem"}},"border":{"width":"0px","style":"none"}}} -->
                <div class="wp-block-group" style="border-style:none;border-width:0px;padding-top:0rem;padding-right:0rem;padding-bottom:0rem;padding-left:0rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Auteur/Autrice</h3>
                <!-- /wp:heading -->
                
                <!-- wp:acf/bloc-contributeurs {"id":"block_62c3fc636da99","name":"acf/bloc-contributeurs","data":{},"align":"","mode":"preview"} /--></div>
                <!-- /wp:group -->
                
                <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
                <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Ressources associées</h3>
                <!-- /wp:heading -->

                <!-- wp:list {"textColor":"primary_turquoise"}-->
                <ul><li class="has-primary-turquoise-color has-text-color">'. $data[10] .'</li></ul>
                <!-- /wp:list --></div>
                <!-- /wp:group --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></main>
                <!-- /wp:group -->'
                ,
                "post_type"     => "news",
                "tax_input"     => array("blog-type"    => array( "event" ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            if ( isset($data[4]) ) {
                // On regarde si l’image existe, sinon on l’insère
                if ( true ) {
                    $id_attachment = import_image( $data[4], $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

function importActualityJobs(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/news-offer.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[11]);

            // Formatage date
            $sepHour = explode(' ', $data[6]);
            $sepHourTemp = array();
            foreach ($sepHour as $key => $value) {
                if (!empty($value)) {
                    $sepHourTemp[] = $sepHour[$key];
                }
            }
            $sepHour = $sepHourTemp;
            $rawDate = $data[5] . ' ' . $sepHour[1];


            $importPost = array(
                "post_title"    => $data[0],
                'post_date'     => $rawDate,
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"35%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:35%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b198cc854e8","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /-->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"bottom","width":"66.66%"} -->
                <div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:66.66%"><!-- wp:post-date /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons -->
                <div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} /--></div>
                <!-- /wp:buttons --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>'. $data[8].$data[9] .'</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color"></h3>
                <!-- /wp:heading -->
                
                <!-- wp:group {"style":{"spacing":{"padding":{"top":"0rem","right":"0rem","bottom":"0rem","left":"0rem"}},"border":{"width":"0px","style":"none"}}} -->
                <div class="wp-block-group" style="border-style:none;border-width:0px;padding-top:0rem;padding-right:0rem;padding-bottom:0rem;padding-left:0rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Auteur/Autrice</h3>
                <!-- /wp:heading -->
                
                <!-- wp:acf/bloc-contributeurs {"id":"block_62c3fc636da99","name":"acf/bloc-contributeurs","data":{},"align":"","mode":"preview"} /--></div>
                <!-- /wp:group -->
                
                <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
                <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Ressources associées</h3>
                <!-- /wp:heading -->

                </div>
                <!-- /wp:group --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></main>
                <!-- /wp:group -->'
                ,
                "post_type"     => "news",
                "tax_input"     => array("blog-type"    => array( "jobs" ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            if ( isset($data[4]) ) {
                // On regarde si l’image existe, sinon on l’insère
                if ( true ) {
                    $id_attachment = import_image( $data[4], $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

//tags
function importTags(){
    if (($handle = fopen(get_template_directory() . "/csv/global-tags.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            wp_insert_term(
                $data[0],
                'global_tags',
                array(
                    'slug'      => $data[0],
                )
            );
        }
        fclose($handle);
    };
}

//realisation
function importRealPublication(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/real-publication.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            $rawDate = $data[4] . ' 12:00';
            $importPost = array(
                "post_title"    => $data[0],
                'post_date'     => $rawDate,
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main">
                
                <!-- wp:acf/hero-real {"id":"block_62bb13e70b971","name":"acf/hero-real","align":"wide","mode":"preview"} /-->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%">
                
                <!-- wp:paragraph -->
                <p>'. $data[6] . $data[7] .'</p>
                <!-- /wp:paragraph -->
                </div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Organisations</h3>
                <!-- /wp:heading -->
                
                <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
                    <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem">
                        <!-- wp:acf/bloc-organisations {"id":"block_62c54d315a683","name":"acf/bloc-organisations","data":{"field_62c5498285f19":""},"align":"","mode":"preview"} /-->
                    </div>
                <!-- /wp:group -->

                <!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Auteur</h3>
                <!-- /wp:heading -->
                
                </div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:group {"style":{"spacing":{"margin":{"top":"2.375em"}}},"className":"post-nav","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
                <div class="wp-block-group post-nav" style="margin-top:2.375em"><!-- wp:post-navigation-link {"type":"previous"} /-->
                
                <!-- wp:post-navigation-link /--></div>
                <!-- /wp:group --></main>
                <!-- /wp:group -->'
                ,
                "post_type"     => "realisations",
                "tax_input"     => array("real-type"    => array( "publication" ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            $imgUrl = explode( ' ', $data[1] );
            $imgUrlClean = str_replace( array('(', ')'), '', $imgUrl);
            $imgUrlCleanEnd = end($imgUrlClean);
            if ( isset($imgUrlCleanEnd) && $imgUrlCleanEnd != '' ) {
                if ( true ) {
                    $id_attachment = import_image( $imgUrlCleanEnd, $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }else if (is_wp_error( $id_attachment )) {
                    $meta['_thumbnail_id'] = null; 
                }
            } else {
                $meta['_thumbnail_id'] = null;
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

function importRealTool(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/real-tool.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            $rawDate = $data[4] . ' 12:00';
            $importPost = array(
                "post_title"    => $data[0],
                'post_date'     => $rawDate,
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main">
                
                <!-- wp:acf/hero-real {"id":"block_62bb13e70b971","name":"acf/hero-real","align":"wide","mode":"preview"} /-->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%">
                
                <!-- wp:paragraph -->
                <p>'. $data[6] . $data[7] .'</p>
                <!-- /wp:paragraph -->
                </div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Organisations</h3>
                <!-- /wp:heading -->
                
                <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
                    <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem">
                        <!-- wp:acf/bloc-organisations {"id":"block_62c54d315a683","name":"acf/bloc-organisations","data":{"field_62c5498285f19":""},"align":"","mode":"preview"} /-->
                    </div>
                <!-- /wp:group -->

                <!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Auteur</h3>
                <!-- /wp:heading -->
                
                </div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:group {"style":{"spacing":{"margin":{"top":"2.375em"}}},"className":"post-nav","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
                <div class="wp-block-group post-nav" style="margin-top:2.375em"><!-- wp:post-navigation-link {"type":"previous"} /-->
                
                <!-- wp:post-navigation-link /--></div>
                <!-- /wp:group --></main>
                <!-- /wp:group -->
                ',

                "post_type"     => "realisations",
                "tax_input"     => array("real-type"    => array( "tool" ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            $imgUrl = explode( ' ', $data[1] );
            $imgUrlClean = str_replace( array('(', ')'), '', $imgUrl);
            $imgUrlCleanEnd = end($imgUrlClean);
            if ( isset($imgUrlCleanEnd) && $imgUrlCleanEnd != '' ) {
                if ( true ) {
                    $id_attachment = import_image( $imgUrlCleanEnd, $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }else if (is_wp_error( $id_attachment )) {
                    $meta['_thumbnail_id'] = null; 
                }
            } else {
                $meta['_thumbnail_id'] = null;
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

//project
function importProject(){   
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/project.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            $importPost = array(
                "post_title"    => $data[0],
                "post_content"  => '
                <!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"20%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:20%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b198cc854e8","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /--></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>' . $data[3] .'</p>
                <!-- /wp:paragraph -->
                </div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:group {"style":{"border":{"style":"solid"},"spacing":{"padding":{"top":"1.5rem","right":"1.5rem","bottom":"1.5rem","left":"1.5rem"}}},"borderColor":"primary_turquoise"} -->
                <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-style:solid;padding-top:1.5rem;padding-right:1.5rem;padding-bottom:1.5rem;padding-left:1.5rem"><!-- wp:paragraph {"textColor":"primary_turquoise"} -->
                <p class="has-primary-turquoise-color has-text-color">' . $data[4] . '</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:group --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                ',
                "post_type"     => "projets",
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );


        }
        fclose($handle);
    };
}

//ecosystem
function importEcoResident(){   
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/ecosystem-resident.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            if ($data[1] === "Permanent") {
                $situation = "actual-resident";
            } else {
                $situation = "old-resident";
            };

            $importPost = array(
                "post_title"    => $data[0],
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"20%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:20%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b44407f8645","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /--></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>' . $data[4] . '</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%">
                <!-- /wp:column --></div>
                <!-- /wp:columns --></main>
                <!-- /wp:group -->
                ',
                "post_type"     => "organisations",
                "tax_input"     => array("neighbor"    => array( $situation ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );


            $imgUrl = explode( ' ', $data[2] );
            $imgUrlClean = str_replace( array('(', ')'), '', $imgUrl);
            $imgUrlCleanEnd = end($imgUrlClean);
            if ( isset($imgUrlCleanEnd) && $imgUrlCleanEnd != '' ) {
                if ( true ) {
                    $id_attachment = import_image( $imgUrlCleanEnd, $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }else if (is_wp_error( $id_attachment )) {
                    $meta['_thumbnail_id'] = null; 
                }
            } else {
                $meta['_thumbnail_id'] = null;
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

function importEcoPartner(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/ecosystem-partner.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            if ($data[1] === "Partenaires métiers") {
                $situation = "partner-job";
            } else if ($data[1] === "Cabinets d'avocats") {
                $situation = "lawyer";
            } else {
                $situation = "software-editor";
            }

            $importPost = array(
                "post_title"    => $data[0],
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"20%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:20%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b44407f8645","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /--></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>' . $data[4] . '</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
                <h3 class="has-primary-turquoise-color has-text-color">Métadonnées</h3>
                <!-- /wp:heading -->
                
                <!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
                <div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:paragraph -->
                <p></p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:group --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></main>
                <!-- /wp:group -->
                ',
                "post_type"     => "organisations",
                "tax_input"     => array("partners"    => array( $situation ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            $imgUrl = explode( ' ', $data[2] );
            $imgUrlClean = str_replace( array('(', ')'), '', $imgUrl);
            $imgUrlCleanEnd = end($imgUrlClean);
            if ( isset($imgUrlCleanEnd) && $imgUrlCleanEnd != '' ) {
                if ( true ) {
                    $id_attachment = import_image( $imgUrlCleanEnd, $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }else if (is_wp_error( $id_attachment )) {
                    $meta['_thumbnail_id'] = null; 
                }
            } else {
                $meta['_thumbnail_id'] = null;
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

function importEcoReference(){
    //return false;
    if (($handle = fopen(get_template_directory() . "/csv/ecosystem-reference.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $globalTags = explode(',', $data[12]);

            $importPost = array(
                "post_title"    => $data[0],
                "post_content"  => '
                <!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
                <main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
                <div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"20%","layout":{"inherit":true}} -->
                <div class="wp-block-column" style="flex-basis:20%"><!-- wp:post-featured-image /--></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b44407f8645","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
                
                <!-- wp:post-title {"level":1} /--></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
                <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
                <p>' . $data[3] . '</p>
                <!-- /wp:paragraph --></div>
                <!-- /wp:column -->
                
                <!-- wp:column {"width":"33.33%"} -->
                <div class="wp-block-column" style="flex-basis:33.33%">
                <!-- /wp:column --></div>
                <!-- /wp:columns --></main>
                <!-- /wp:group -->
                ',
                "post_type"     => "organisations",
                "tax_input"     => array("references"    => array( 'reference' ),
                                        "global_tags"   => $globalTags
                ),
                "post_status"   => "publish"
            );
            $p = wp_insert_post( $importPost, true );

            $imgUrl = explode( ' ', $data[1] );
            $imgUrlClean = str_replace( array('(', ')'), '', $imgUrl);
            $imgUrlCleanEnd = end($imgUrlClean);
            if ( isset($imgUrlCleanEnd) && $imgUrlCleanEnd != '' ) {
                if ( true ) {
                    $id_attachment = import_image( $imgUrlCleanEnd, $p );
                }
                if ( ! is_wp_error( $id_attachment ) ) {
                    $meta['_thumbnail_id'] = $id_attachment;
                }else if (is_wp_error( $id_attachment )) {
                    $meta['_thumbnail_id'] = null; 
                }
            } else {
                $meta['_thumbnail_id'] = null;
            }
            if ( ! empty( $meta ) ) {
                foreach ( $meta as $k => $m ) {
                    update_post_meta( $p, $k, $m );
                }
            }
        }
        fclose($handle);
    };
}

?>