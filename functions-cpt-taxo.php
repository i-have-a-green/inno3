<?php

add_action( 'init', 'ihag_custom_post_property' );
function ihag_custom_post_property() {

	register_post_type(
		'realisations', 
		array(
			'labels'             => array(
				'name'          => __( 'Réalisations', 'ihag' ),
				'singular_name' => __( 'Réalisation', 'ihag' ),
			),
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-portfolio',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'realisation', 'with_front' => false ),
			'show_in_rest'       => true,
			'has_archive'        => true,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
			
		)
	);
	$args = array(
		'label'        => __( 'Type de réalisation', 'ihag' ),
		'hierarchical' => false,
		'show_in_rest' => true
	);
	register_taxonomy( 'real-type', 'realisations', $args );


	register_post_type(
		'projets', 
		array(
			'labels'             => array(
				'name'          => __( 'Projets', 'ihag' ),
				'singular_name' => __( 'Projet', 'ihag' ),
			),
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-portfolio',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'projets', 'with_front' => false ),
			'show_in_rest'       => true,
			'has_archive'        => false,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
			
		)
	);
	$args = array(
		'label'        => __( 'Type de projet', 'ihag' ),
		'hierarchical' => false,
		'show_in_rest' => true
	);
	register_taxonomy( 'project-type', 'projets', $args );



	register_post_type(
		'organisations', 
		array(
			'labels'             => array(
				'name'          => __( 'Organisations', 'ihag' ),
				'singular_name' => __( 'Organisation', 'ihag' ),
			),
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-portfolio',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'organisations', 'with_front' => false  ),
			'show_in_rest'       => true,
			'has_archive'        => false,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
			
		)
	);
	$args = array(
		'label'        => __( 'Références', 'ihag' ),
		'hierarchical' => false,
        'public'       => true,
		'show_in_rest' => true,
        'rewrite'      => array( 'slug' => 'references', 'with_front' => false  ),
        //http://inno3.local/references/reference/
	);
	register_taxonomy( 'references', 'organisations', $args );


	$args = array(
		'label'        => __( 'Partenaires', 'ihag' ),
		'hierarchical' => false,
		'show_in_rest' => true,
        'rewrite'      => array( 'slug' => 'partners', 'with_front' => false  ),
	);
	register_taxonomy( 'partners', 'organisations', $args );


	$args = array(
		'label'        => __( 'Voisins(137)', 'ihag' ),
		'hierarchical' => false,
		'show_in_rest' => true,
        'rewrite'      => array( 'slug' => 'neighbor', 'with_front' => false  ),
	);
	register_taxonomy( 'neighbor', 'organisations', $args );

    register_taxonomy( 'skill', 'users' );
    register_taxonomy( 'situation', 'users' );

	register_post_type(
		'news', 
		array(
			'labels'             => array(
				'name'          => __( 'Actualités', 'ihag' ),
				'singular_name' => __( 'Actualité', 'ihag' ),
			),
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-portfolio',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'blog', 'with_front' => false  ),
			'show_in_rest'       => true,
			'has_archive'        => true,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
			
		)
	);
	$args = array(
		'label'        => __( 'Type d\'actualité', 'ihag' ),
		'hierarchical' => false,
		'show_in_rest' => true,
		
	);
	register_taxonomy( 'blog-type', 'news', $args );


	/* $posts = get_posts( array(
		'posts_per_page'    => -1,
		'post_type'         => 'post',
		'post_status'       => 'any',
	) );

	foreach($posts as $news){
		$news->post_type = 'news';
		wp_update_post($news);
	}*/


	$args = array(
		'label'        => __( 'Sujets (tags)', 'ihag' ),
		'hierarchical' => false  ,
		'show_in_rest' => true,
	);
	register_taxonomy( 'global_tags', array('projets', 'news', 'organisations', 'realisations'), $args );

    /*global $wp_rewrite;
    $wp_rewrite->flush_rules();*/
}

add_action('admin_menu', 'remove_unnecessary_wordpress_menus', 999);

function remove_unnecessary_wordpress_menus(){
    remove_menu_page( 'edit.php' );
}



