<?php

function update_post_type_support() {
	// add_post_type_support( 'page', 'excerpt' );
	// remove_post_type_support( 'page', 'thumbnail' );
}
add_action( 'init', 'update_post_type_support' );

function ihag_unregister_taxonomy() {
	register_taxonomy( 'post_tag', array() );
	register_taxonomy( 'category', array() );
}
add_action( 'init', 'ihag_unregister_taxonomy' );


add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );
function remove_thumbnail_dimensions( $html ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', '', $html );
	$html = preg_replace( '/style="[^\"]*"/', '', $html );
	// $html = preg_replace('/sizes="[^\"]*"/', '', $html);
	return $html;
}

function ihag_get_term( $post, $taxonomy ) {
	if ( class_exists( 'WPSEO_Primary_Term' ) ) :
		$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, get_the_id( $post ) );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term               = get_term( $wpseo_primary_term );
		if ( is_wp_error( $term ) ) {
			$term = get_the_terms( $post, $taxonomy );
			return $term[0];
		}
		return $term;
	else :
		$term = get_the_terms( $post, $taxonomy );
		return $term[0];
	endif;
}


function target_main_category_query_with_conditional_tags( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {
		// Not a query for an admin page.
		// It's the main query for a front end page of your site.
		if ( is_tax( 'taxo_training' ) && isset( $_GET['var_taxo_tag'] ) && ! empty( $_GET['var_taxo_tag'] ) ) {
			// It's the main query for a category archive.
			// Let's change the query for category archives.
			$query->set(
				'tax_query',
				array(
					array(
						'taxonomy' => 'taxo_tag',
						'field'    => 'term_id',
						'terms'    => $_GET['var_taxo_tag'],
					),
				)
			);
		}
	}
}
add_action( 'pre_get_posts', 'target_main_category_query_with_conditional_tags' );



function nbTinyURL( $url ) {
	$ch      = curl_init();
	$timeout = 5;
	curl_setopt( $ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
	$data = curl_exec( $ch );
	curl_close( $ch );
	return $data;
}  
