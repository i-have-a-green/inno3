<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Armando
 * @since 1.0.0
 */

/**
 * The theme version.
 *
 * @since 1.0.0
 */
define( 'IHAG_VERSION', wp_get_theme()->get( 'Version' ) );

/** Check if the WordPress version is 5.5 or higher, and if the PHP version is at least 7.2. If not, do not activate. */
if ( version_compare( $GLOBALS['wp_version'], '5.5', '<' ) || version_compare( PHP_VERSION_ID, '70200', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

require get_template_directory() . '/functions-cpt-taxo.php';

/**
 * Adds theme-supports.
 *
 * @since 1.2.4
 * @return void
 */
function armando_setup() {
	// Add support for Block Styles.
	add_theme_support( 'wp-block-styles' );
	// Enqueue editor styles.
	add_theme_support( 'editor-styles' );

	add_theme_support( 'custom-units');

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	add_editor_style(
/* 		array(
			'./assets/css/style-shared.css',
		) */
		"style.css"
	);


	/*
	* Enable support for Post Thumbnails on posts and pages.
	*
	* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	*/
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1568, 9999 );
	add_image_size( '50-50', 72, 72 );
	add_image_size( '150-150', 100, 100 );
	add_image_size( '350-350', 350, 350 );
	add_image_size( '450-450', 450, 450 );
	add_image_size( '650-650', 650, 650 );
	add_image_size( '850-850', 850, 850 );

	function ihag_custom_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'50-50' => __( '50x50 - Socials small', 'ihag' ),
				'150-150' => __( '150x150 - Petite sans rognage', 'ihag' ),
				'350-350' => __( '350x350 - Contact sans rognage', 'ihag' ),
				'450-450' => __( '450x450 - Moyen sans rognage', 'ihag' ),
				'650-650' => __( '650x650 - Grande sans rognage', 'ihag' ),
				'650-650' => __( '850x850 - Grande sans rognage', 'ihag' ),
			)
		);
	}
	add_filter( 'image_size_names_choose', 'ihag_custom_sizes' );


	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );
}
add_action( 'after_setup_theme', 'armando_setup' );


/**
 * Show '(No title)' if post has no title.
 */
add_filter(
	'the_title',
	function( $title ) {
		if ( ! is_admin() && empty( $title ) ) {
			$title = __( '(No title)', 'armando' );
		}

		return $title;
	}
);


/**
 * Propose les plugins à télécharger.
 */
require get_template_directory() . '/inc/plugin-require.php';

/**
 * functions pour le block breadcrumb.
 */
require get_template_directory() . '/inc/breadcrumb.php';

/**
 *  ACF Field.
 */
require get_template_directory() . '/inc/acf-block.php';

/**
 *  ACF Field styles.
 */
require get_template_directory() . '/inc/acf-style.php';

/**
 * Enqueue scripts and styles.
 */
function ihag_scripts() {
	wp_enqueue_style( 'ihag-style', get_stylesheet_uri(), array(), IHAG_VERSION );

	wp_enqueue_script( 'ihag-script', get_template_directory_uri() . '/js/script.js', array(), IHAG_VERSION, true );
/* 	wp_localize_script('ihag-resturl', 'resturl', array (
		'resturl' => site_url() . '/wp-json/ihag/'
	)); */
/* 	wp_localize_script( 'ihag-nonce', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ),
		'nonce' => wp_create_nonce( 'wp_rest' )
	) ); */

}
add_action( 'wp_enqueue_scripts', 'ihag_scripts' );

/**
 * My_acf_json_save_point
 *
 * @param  mixed $path
 * @return string
 */
function ihag_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/jsonACF';
	if ( ! file_exists( $path ) ) {
		mkdir( $path, 0777 );}
	return $path;
}
add_filter( 'acf/settings/save_json', 'ihag_acf_json_save_point' );


/**
 * My_acf_json_load_point
 *
 * @param  mixed $paths
 * @return string
 */
function ihag_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/jsonACF';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'ihag_acf_json_load_point' );


if ( ! defined( 'WP_POST_REVISIONS' ) ) {
	define( 'WP_POST_REVISIONS', 3 );
}


/**
 * Ihag_revision_number
 *
 * @param  mixed $num
 * @param  mixed $post
 * @return int
 */
function ihag_revision_number( $num, $post ) {
	return 3;
}
add_filter( 'wp_revisions_to_keep', 'ihag_revision_number', 4, 2 );

// un intervalle entre deux sauvegardes de 360 secondes.
if ( ! defined( 'AUTOSAVE_INTERVAL' ) ) {
	define( 'AUTOSAVE_INTERVAL', 360 );
}

/**
 * Ihag_clean_head
 * 
 * @link https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
 * 
 * @return void
 */
function ihag_clean_head() {
	return '';
}
add_filter( 'the_generator', 'ihag_clean_head' );

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds.
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed.


add_filter( 'default_content', 'add_default_content_to_cpt', 10, 2 );

// remove default pattern
add_action('init', function() {
	remove_theme_support('core-block-patterns');
});

//add custom categories
function my_plugin_register_my_pattern_categories() {
    register_block_pattern_category(
        'global',
        array( 'label' => __( 'Global', 'inno3' ) )
    );
    register_block_pattern_category(
        'blog',
        array( 'label' => __( 'Blog', 'inno3' ) )
    );
    register_block_pattern_category(
        'contact',
        array( 'label' => __( 'Contact', 'inno3' ) )
    );
    register_block_pattern_category(
        'realisation',
        array( 'label' => __( 'Réalisations', 'inno3' ) )
    );
    register_block_pattern_category(
        'home',
        array( 'label' => __( 'Accueil', 'inno3' ) )
    );
    register_block_pattern_category(
        'offer',
        array( 'label' => __( 'Offre(s)', 'inno3' ) )
    );
    register_block_pattern_category(
        'project',
        array( 'label' => __( 'Projet(s)', 'inno3' ) )
    );
}
add_action( 'init', 'my_plugin_register_my_pattern_categories' );

function add_default_content_to_cpt( $content, $post ) {
	
	if ($post->post_type == 'post') {
		return '
		<!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
		<main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
		<div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"35%","layout":{"inherit":true}} -->
		<div class="wp-block-column" style="flex-basis:35%"><!-- wp:post-featured-image /--></div>
		<!-- /wp:column -->

		<!-- wp:column -->
		<div class="wp-block-column"><!-- wp:post-title {"level":1} /-->

		<!-- wp:columns -->
		<div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"bottom","width":"66.66%"} -->
		<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:66.66%"><!-- wp:post-date /--></div>
		<!-- /wp:column -->

		<!-- wp:column {"width":"33.33%"} -->
		<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-bg-b"} /--></div>
		<!-- /wp:buttons --></div>
		<!-- /wp:column --></div>
		<!-- /wp:columns --></div>
		<!-- /wp:column --></div>
		<!-- /wp:columns -->

		<!-- wp:columns -->
		<div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
		<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
		<p></p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p></p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p></p>
		<!-- /wp:paragraph --></div>
		<!-- /wp:column -->

		<!-- wp:column {"width":"33.33%"} -->
		<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
		<h3 class="has-primary-turquoise-color has-text-color">Auteur</h3>
		<!-- /wp:heading -->

		<!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
		<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:post-author {"byline":"By daniel"} /-->

		<!-- wp:buttons {"layout":{"type":"flex","orientation":"vertical"}} -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} /-->

		<!-- wp:button {"className":"is-style-ol-b"} /-->

		<!-- wp:button {"className":"is-style-ol-b"} /--></div>
		<!-- /wp:buttons --></div>
		<!-- /wp:group -->

		<!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
		<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
		<h3 class="has-primary-turquoise-color has-text-color"></h3>
		<!-- /wp:heading -->

		<!-- wp:buttons {"layout":{"type":"flex","orientation":"vertical"}} -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-ol-b"} /-->

		<!-- wp:button {"className":"is-style-ol-b"} /-->

		<!-- wp:button {"className":"is-style-ol-b"} /--></div>
		<!-- /wp:buttons --></div>
		<!-- /wp:group --></div>
		<!-- /wp:column --></div>
		<!-- /wp:columns -->

		<!-- wp:group {"style":{"spacing":{"margin":{"top":"2.375em"}}},"className":"post-nav","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
		<div class="wp-block-group post-nav" style="margin-top:2.375em"><!-- wp:post-navigation-link {"type":"previous"} /-->

		<!-- wp:post-navigation-link /--></div>
		<!-- /wp:group --></main>
		<!-- /wp:group -->
	';
	}
	elseif ($post->post_type == 'organisations') {
		return '
		<!-- wp:group {"tagName":"main","align":"full","className":"site-main","layout":{"inherit":true}} -->
		<main class="wp-block-group alignfull site-main"><!-- wp:columns {"align":"wide","className":"single-header"} -->
		<div class="wp-block-columns alignwide single-header"><!-- wp:column {"width":"20%","layout":{"inherit":true}} -->
		<div class="wp-block-column" style="flex-basis:20%"><!-- wp:post-featured-image /--></div>
		<!-- /wp:column -->
		
		<!-- wp:column -->
		<div class="wp-block-column"><!-- wp:acf/breadcrumb {"id":"block_62b44407f8645","name":"acf/breadcrumb","align":"","mode":"preview"} /-->
		
		<!-- wp:post-title {"level":1} /--></div>
		<!-- /wp:column --></div>
		<!-- /wp:columns -->
		
		<!-- wp:columns -->
		<div class="wp-block-columns"><!-- wp:column {"width":"66.66%"} -->
		<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:paragraph -->
		<p></p>
		<!-- /wp:paragraph --></div>
		<!-- /wp:column -->
		
		<!-- wp:column {"width":"33.33%"} -->
		<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:heading {"level":3,"textColor":"primary_turquoise"} -->
		<h3 class="has-primary-turquoise-color has-text-color">Métadonnées</h3>
		<!-- /wp:heading -->
		
		<!-- wp:group {"style":{"border":{"width":"2px"},"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}}},"borderColor":"primary_turquoise"} -->
		<div class="wp-block-group has-border-color has-primary-turquoise-border-color" style="border-width:2px;padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:paragraph -->
		<p></p>
		<!-- /wp:paragraph --></div>
		<!-- /wp:group --></div>
		<!-- /wp:column --></div>
		<!-- /wp:columns --></main>
		<!-- /wp:group -->
	';
	}

}



require get_template_directory() . '/functions-import.php';


/************* Page option *******************/
if( function_exists('acf_add_options_page') ) {
	// Page principale
	acf_add_options_page(array(
		'page_title'    => 'Options',
		'menu_title'    => 'Options',
		'menu_slug'     => 'options-generales',
		'capability'    => 'edit_posts',
		'redirect'      => true
	));

  // Page d'options
  acf_add_options_sub_page(array(
  	'page_title' 	=> 'Options Générales',
  	'menu_slug' 	=> 'acf_options',
  	'parent_slug'   => 'options-generales'
  ));
}

function excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'excerpt_length');